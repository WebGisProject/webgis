$(function () {
	globle.init();
});

(function (owner) {
	owner.options = {
		center: {
			lat: 34.260985082636445,
			lng: 108.94236087799074
		},
	};
	/*初始化*/
	owner.init = function () {
		this.customLayersPlugin = new L.CustomLayersPlugin();
		this.initMap();
	};
	/* 初始化地图 */
	owner.initMap = function () {
		this.map = L.map('map', {
			center: this.options.center,
			zoom: 5,
			layers: [this.customLayersPlugin.options.layers.GaoDe.GaoDeNormalMap],
			doubleClickZoom: false
		});
		L.control.layers(this.customLayersPlugin._layers(), {}, {
			position: "bottomright"
		}).addTo(this.map);
		this.customToolPlugin = new L.CustomToolPlugin(this.map);
	};
	/* 添加popup-坐标拾取 */
	owner.addPopup = function (e) {
		var latlng = e.latlng;
		var popup = L.popup().setLatLng(latlng).setContent(JSON.stringify(latlng));
		owner.options.layerGroup.addLayer(popup);
	};
	/* 加载geojson */
	owner.loadGeoJSON = function () {
		var geojsonFeature = {
			"type": "Feature",
			"properties": {
				"name": "Coors Field",
				"amenity": "Baseball Stadium",
				"popupContent": "落基山脉就在这里玩耍!"
			},
			"geometry": {
				"type": "Point",
				"coordinates": [108.98368835449219, 34.24430424837769]
			}
		}
		owner.options.layerGroupPoint.addLayer(L.geoJSON(geojsonFeature));
	};
}(window.globle = {}));


(function (owner) {
	/*提示信息*/
	owner.message = function (title, type) {
		new $.zui.Messager(title, {
			type: type ? type : 'primary',
			icon: "info-sign"
		}).show();
	};
}(window.zuiPlus = {}));