/**
* Author: 张皓辰
* Date: 2019年3月28日下午3:40:49
*/
L.CustomLayersPlugin = L.Class.extend({
	options: {
		show: true,
		layers: {
			Geoq: {
				GeoqNormalMap: L.tileLayer.chinaProvider('Geoq.Normal.Map', {
					maxZoom: 18,
					minZoom: 4
				}),
				GeoqNormalPurplishBlue: L.tileLayer.chinaProvider('Geoq.Normal.PurplishBlue', {
					maxZoom: 18,
					minZoom: 4
				}),
				GeoqNormalGray: L.tileLayer.chinaProvider('Geoq.Normal.Gray', {
					maxZoom: 18,
					minZoom: 4
				}),
				GeoqNormalWarm: L.tileLayer.chinaProvider('Geoq.Normal.Warm', {
					maxZoom: 18,
					minZoom: 4
				}),
				GeoqNormalHydro: L.tileLayer.chinaProvider('Geoq.Theme.Hydro', {
					maxZoom: 18,
					minZoom: 4
				})
			},
			TianDiTu: {
				TianDiTuNormalMap: L.tileLayer.chinaProvider('TianDiTu.Normal.Map', {
					maxZoom: 18,
					minZoom: 4
				}),
				TianDiTuNormalAnnotion: L.tileLayer.chinaProvider('TianDiTu.Normal.Annotion', {
					maxZoom: 18,
					minZoom: 4
				}),
				TianDiTuImgem: L.tileLayer.chinaProvider('TianDiTu.Satellite.Map', {
					maxZoom: 18,
					minZoom: 4
				}),
				TianDiTuImage: L.tileLayer.chinaProvider('TianDiTu.Satellite.Annotion', {
					maxZoom: 18,
					minZoom: 4
				}),
			},
			Google: {
				GoogleNormalMap: L.tileLayer.chinaProvider('Google.Normal.Map', {
					maxZoom: 18,
					minZoom: 4
				}),
				GoogleSatelliteMap: L.tileLayer.chinaProvider('Google.Satellite.Map', {
					maxZoom: 18,
					minZoom: 4
				})
			},
			GaoDe: {
				GaoDeNormalMap: L.tileLayer.chinaProvider('GaoDe.Normal.Map', {
					maxZoom: 18,
					minZoom: 4
				}),
				GaoDeImgem: L.tileLayer.chinaProvider('GaoDe.Satellite.Map', {
					maxZoom: 18,
					minZoom: 4
				}),
				GaoDeImage: L.tileLayer.chinaProvider('GaoDe.Satellite.Annotion', {
					maxZoom: 18,
					minZoom: 4
				})
			}
		}
	},
	initialize: function (options) {
		L.setOptions(this, options);
	},
	/* 调用外部底图 */
	_layers: function () {
		var TianDiTuNormal = L.layerGroup([this.options.layers.TianDiTu.TianDiTuNormalMap, this.options.layers.TianDiTu.TianDiTuNormalAnnotion]),
			TianDiTuImage = L.layerGroup([this.options.layers.TianDiTu.TianDiTuImgem, this.options.layers.TianDiTu.TianDiTuImage]),
			GaoDeImage = L.layerGroup([this.options.layers.GaoDe.GaoDeImgem, this.options.layers.GaoDe.GaoDeImage]);
		if (this.options.show) {
			return {
				"智图地图": this.options.layers.Geoq.GeoqNormalMap,
				"智图午夜蓝": this.options.layers.Geoq.GeoqNormalPurplishBlue,
				"智图灰色": this.options.layers.Geoq.GeoqNormalGray,
				"智图暖色": this.options.layers.Geoq.GeoqNormalWarm,
				"智图冷色": this.options.layers.Geoq.GeoqNormalHydro,
				"天地图": TianDiTuNormal,
				"天地图影像": TianDiTuImage,
				"谷歌地图": this.options.layers.Google.GoogleNormalMap,
				"谷歌影像": this.options.layers.Google.GoogleSatelliteMap,
				"高德地图": this.options.layers.GaoDe.GaoDeNormalMap,
				"高德影像": GaoDeImage
			}
		} else {
			return {};
		}
	}
});