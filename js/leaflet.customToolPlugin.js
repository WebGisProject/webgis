L.CustomToolPlugin = L.Class.extend({
    options: {
        name: "工具栏",
        isTool: null,
        center: {
            lat: 34.260985082636445,
            lng: 108.94236087799074
        },
        toolData: [
            {
                name: "点",
                icon: "icon-marker",
                clazz: "point"
            },
            {
                name: "线",
                icon: "icon-line_draw",
                clazz: "line"
            },
            {
                name: "多边形",
                icon: "icon-polygon_draw",
                clazz: "polygon"
            },
            {
                name: "矩形",
                icon: "icon-mian",
                clazz: "rectangle"
            },
            {
                name: "拉框放大",
                icon: "icon-lakuangfangda",
                clazz: "maxZoom"
            },
            {
                name: "拉框缩小",
                icon: "icon-lakuangsuoxiao",
                clazz: "minZoom"
            },
            {
                name: "全国",
                icon: "icon-artboard19",
                clazz: "country"
            },
            {
                name: "全屏",
                icon: "icon-quanping",
                clazz: "fullScreen"
            },
            {
                name: "清除",
                icon: "icon-qingchu",
                clazz: "clear"
            }
        ]
    },
    initialize: function (map, options) {
        this._map = map;
        this._container = map._container;
        L.setOptions(this, options);
        this._initTool();
    },
    /* 初始化工具栏 */
    _initTool: function () {
        this._createToolDom();
        this._createLayerGroup();
        this._initDrawTool();
    },
    _createLayerGroup: function () {
        this.layerGroupPoint = L.layerGroup().addTo(this._map);
        this.layerGroupLine = L.layerGroup().addTo(this._map);
        this.layerGroupRectangle = L.layerGroup().addTo(this._map);
        this.layerGroupPolygon = L.layerGroup().addTo(this._map);
    },
    /* 创建工具栏 */
    _createToolDom: function () {
        var _Div = L.DomUtil.create('div');
        var _ul = L.DomUtil.create('ul');
        var msg = this.options.toolData;
        for (var i = 0; i < msg.length; i++) {
            var _li = L.DomUtil.create('li', 'customTool_li customTool_' + msg[i].clazz);
            var _i = L.DomUtil.create('i', 'iconfont ' + msg[i].icon);
            _li.title = msg[i].name;
            _li.id = msg[i].clazz;
            _li.type = msg[i].clazz;
            _li.setAttribute('data-toggle', "tooltip");
            _li.setAttribute('data-tip-class', "tooltip-primary");

            _li.appendChild(_i);
            _ul.appendChild(_li);
        }
        this._insertDom(_Div, _ul);
    },
    /* 向地图中插入工具栏 */
    _insertDom: function (_Div, _ul) {
        if (this.options.isTool) {
            this.options.isTool.appendChild(_ul);
        } else {
            _Div.className = "customTool_toolBox";
            this._container.appendChild(_Div);
            _Div.appendChild(_ul);
        }
        $('.customTool_li').tooltip({
            tipClass: 'tooltip-primary',
            placement: 'left',
            container: 'body'
        });
        var dom = document.querySelector('.customTool_toolBox');
        this._toolStyle(dom);
        var nodes = dom.childNodes[0].childNodes;
        for (var i = 0; i < nodes.length; i++) {
            this._toolMouse(nodes[i]);
        }
        var _this = this;
        window.onresize = function () {
            _this._toolStyle(dom);
        }
    },
    /* 工具栏样式 */
    _toolStyle: function (dom) {
        dom.style.top = (document.documentElement.clientHeight - dom.offsetHeight) / 2 + "px";
    },
    /* 工具栏绑定事件 */
    _toolMouse: function (wordNode) {
        var _this = this;
        wordNode.onmouseover = function () {
            _this._map.doubleClickZoom.disable();
            _this._map.scrollWheelZoom.disable();
        };
        wordNode.onmouseout = function () {
            _this._map.doubleClickZoom.enable();
            _this._map.scrollWheelZoom.enable();
        };
        L.DomEvent
            .addListener(wordNode, 'click', L.DomEvent.stopPropagation)
            .addListener(wordNode, 'click', L.DomEvent.preventDefault)
            .addListener(wordNode, 'click', this._toolChildNodesHandle, this);
    },
    /* 工具栏操作事件 */
    _toolChildNodesHandle: function (e) {
        var element = $(e.currentTarget);
        element.addClass("active").siblings().removeClass("active");
        switch (element.attr('id')) {
            case 'point': //点
                this._drawPoint();
                break;
            case 'line': //线
                this._drawPolyline(element);
                break;
            case 'polygon': //多边形
                this._drawPolygon(element);
                break;
            case 'rectangle': //矩形
                this._drawRectangle();
                break;
            case 'maxZoom': //拉框放大
                this._dropMaxZoom();
                break;
            case 'minZoom': //拉框缩小
                this._dropMinZoom();
                break;
            case 'country': //全国
                this._scalingCountry(element);
                break;
            case 'fullScreen': //全屏
                this._showFullScreen(element);
                break;
            case 'clear': //清除
                element.removeClass("active").siblings().removeClass("active");
                this._clearLayers();
                break;
        }
    },
    /* 添加marker */
    _addMarker: function (latlng) {
        var marker = L.marker(latlng);
        this.layerGroupPoint.addLayer(marker);
        marker.bindPopup("_leaflet_id：" + marker._leaflet_id).openPopup();
    },
    /* 添加线 */
    _addPolyline: function (latlng) {
        var polyline = L.polyline(latlng, {
            color: 'red',
            weight: 3
        });
        this.layerGroupLine.addLayer(polyline);
        polyline.bindPopup("_leaflet_id：" + polyline._leaflet_id).openPopup();
    },
    /* 添加多边形 */
    _addPolygon: function (latlng) {
        var polygon = L.polygon(latlng, {
            color: 'red',
            weight: 3
        });
        this.layerGroupPolygon.addLayer(polygon);
        polygon.bindPopup("_leaflet_id：" + polygon._leaflet_id);
    },
    /* 添加矩形 */
    _addRectangle: function (latlng) {
        var rectangle = L.rectangle(latlng, {
            color: 'red',
            weight: 3
        });
        this.layerGroupRectangle.addLayer(rectangle);
        rectangle.bindPopup("_leaflet_id：" + rectangle._leaflet_id);
        this._clearMeasureTool();
        $(".customTool_toolBox ul li").removeClass('active');
    },
    /*初始化绘制工具*/
    _initDrawTool: function () {
        var _that = this;
        /*绘制线和多边形*/
        this.MeasureTool = new L.MeasureTool(this._map, {
            color: 'red',
            weight: 3
        });
        /*绘制矩形*/
        L.customBoxSelect = L.BoxSelect.extend({
            _onMouseUp: function () {
                this._DomDownUp("off");
                this._finish(!0);
                this.rectangle();
            },
            //绘制一个矩形框
            rectangle: function () {
                var startPoint = this._map.layerPointToLatLng(this._startPoint);
                var endPoint = this._map.layerPointToLatLng(this._endPoint);
                var bounds = [startPoint, endPoint];
                if (this.options.type == "maxZoom") {
                    _that._judgeZoom(true);
                } else if (this.options.type == "minZoom") {
                    _that._judgeZoom();
                } else {
                    _that._addRectangle(bounds);
                    zuiPlus.message('矩形绘制完成');
                }
                $(".customTool_toolBox ul li").removeClass('active');
            }
        });
        this.BoxSelect = new L.customBoxSelect(this._map);
        this.BoxSelectMaxZoom = new L.customBoxSelect(this._map, {type: "maxZoom"});
        this.BoxSelectMinZoom = new L.customBoxSelect(this._map, {type: "minZoom"});
    },
    /*判断级别*/
    _judgeZoom: function (type) {
        var maxZoom = this._map.getMaxZoom(),
            zoom = this._map.getZoom(),
            minZoom = this._map.getMinZoom();

        if (zoom == maxZoom) {
            if (!type) {
                zoom--;
            } else {
                zuiPlus.message('地图缩放已达最大级别');
            }
        } else if (zoom == minZoom) {
            if (type) {
                zoom++;
            } else {
                zuiPlus.message('地图缩放已达最小级别');
            }
        } else {
            type ? zoom++ : zoom--;
        }
        this._map.setZoom(zoom);
        zuiPlus.message('地图当前缩放级别为：' + zoom + '级');
        this._clearMeasureTool();
    },
    /*绘制点*/
    _drawPoint: function () {
        this._clearMeasureTool();
        this._setDrawMapStyle();
        this._map.on('click', this._offMapClick, this);
    },
    /*关闭地图事件*/
    _offMapClick: function (e) {
        this._addMarker(e.latlng);
        this._setDefaultMapStyle(function () {
            $(".customTool_toolBox ul li").removeClass('active');
            zuiPlus.message('点标记完成');
        });
        this._map.off('click', this._offMapClick, this);
    },
    /*绘制线*/
    _drawPolyline: function (_this) {
        var _that = this;
        this._clearMeasureTool();
        this._setDrawMapStyle();
        this.MeasureTool.enable();
        this._map.on('dblclick', function (e) {
            var polyline = _that.MeasureTool.geometry._latlngs;
            _that._addPolyline(polyline);
            _that.MeasureTool.disable();
            _that._setDefaultMapStyle();
            $(_this).removeClass("active");
            zuiPlus.message('线绘制完成');
        });
    },
    /*绘制多边形*/
    _drawPolygon: function (_this) {
        var _that = this;
        this._clearMeasureTool();
        this._setDrawMapStyle();
        this.MeasureTool.enable();
        this.MeasureTool.options.closed = true;
        this._map.on('dblclick', function (e) {
            var polygon = _that.MeasureTool.geometry._latlngs;
            _that._addPolygon(polygon);
            _that.MeasureTool.disable();
            _that._setDefaultMapStyle(function () {
                _that.MeasureTool.options.closed = false;
                $(_this).removeClass("active");
                zuiPlus.message('多边形绘制完成');
            });
        });
    },
    /*绘制矩形*/
    _drawRectangle: function () {
        this._clearMeasureTool();
        this._setDrawMapStyle();
        this.BoxSelect.enable();
    },
    /*拉框放大*/
    _dropMaxZoom: function () {
        this._clearMeasureTool();
        this._setDrawMapStyle();
        this.BoxSelectMaxZoom.enable();
    },
    /*拉框缩小*/
    _dropMinZoom: function () {
        this._clearMeasureTool();
        this._setDrawMapStyle();
        this.BoxSelectMinZoom.enable();
    },
    /*缩放全国*/
    _scalingCountry: function (_this) {
        this._map.setView(this.options.center, 5);
        $(_this).removeClass("active");
        zuiPlus.message('已缩放至全国范围');
    },
    /*全屏查看*/
    _showFullScreen: function (_this) {
        if (this._map.isFullscreen()) {
            zuiPlus.message('已关闭全屏模式');
        } else {
            zuiPlus.message('已打开全屏模式');
        }
        this._map.toggleFullscreen();
        $(_this).removeClass('active');
    },
    /*清除地图绘制*/
    _clearMeasureTool: function () {
        this.MeasureTool.disable();
        this.MeasureTool.options.closed = false;
        this.BoxSelect.disable();
        this.BoxSelectMaxZoom.disable();
        this.BoxSelectMinZoom.disable();
        this._map.off('click', this._offMapClick, this);
        this._setDefaultMapStyle();
    },
    /*清除所有*/
    _clearLayers: function () {
        this.layerGroupPoint.clearLayers();
        this.layerGroupLine.clearLayers();
        this.layerGroupRectangle.clearLayers();
        this.layerGroupPolygon.clearLayers();
        this._clearMeasureTool();
        zuiPlus.message('地图上所有数据清除完成');
    },
    /*默认地图样式*/
    _setDefaultMapStyle: function (callback) {
        this._map.doubleClickZoom.enable();
        this._map._container.style.cursor = "";
        this._map.off('dblclick');
        if (callback) callback();
    },
    /*绘制地图样式*/
    _setDrawMapStyle: function () {
        this._map.doubleClickZoom.disable();
        this._map._container.style.cursor = "crosshair";
    }
});