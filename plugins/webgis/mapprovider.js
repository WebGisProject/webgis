﻿var MapProvider =
    [
        {
            name: "地理",
            image: "images/geographic.png",
            minzoom: 0,
            maxzoom: 18,
            subdomains: "01234567",
            copyright: "<a href='http://map.tianditu.com/map/index.html' target='_blank'><img alt='' src='http://api.tianditu.com/img/map/logo.png' border='0'/></a>",
            baselayer: "http://t{s}.tianditu.cn/DataServer?T=vec_w&X={x}&Y={y}&L={z}",
            overlayers:
            [
                {
                    overlayer: "http://t{s}.tianditu.cn/DataServer?T=cva_w&X={x}&Y={y}&L={z}"
                }
            ]
        }
        ,
        {
            name: "遥感",
            image: "images/satellite.png",
            minzoom: 0,
            maxzoom: 18,
            subdomains: "01234567",
            copyright: "<a href='http://map.tianditu.com/map/index.html' target='_blank'><img alt='' src='http://api.tianditu.com/img/map/logo.png' border='0'/></a>",
            baselayer: "http://t{s}.tianditu.cn/DataServer?T=img_w&X={x}&Y={y}&L={z}",
            overlayers:
            [
                {
                    overlayer: "http://t{s}.tianditu.cn/DataServer?T=cia_w&X={x}&Y={y}&L={z}"
                }
            ]
        }
        ,
        {
            name: "地势",
            image: "images/mountain.png",
            minzoom: 0,
            maxzoom: 18,
            subdomains: "01234567",
            copyright: "<a href='http://map.tianditu.com/map/index.html' target='_blank'><img alt='' src='http://api.tianditu.com/img/map/logo.png' border='0'/></a>",
            baselayer: "http://t{s}.tianditu.cn/DataServer?T=ter_w&X={x}&Y={y}&L={z}",
            overlayers:
            [
                {
                    overlayer: "http://t{s}.tianditu.cn/wat_w/wmts?service=wmts&request=GetTile&version=1.0.0&LAYER=wat&tileMatrixSet=w&TileMatrix={z}&TileRow={y}&TileCol={x}&style=default&format=tiles"
                }
                ,
                {
                    overlayer: "http://t{s}.tianditu.cn/bou_w/wmts?service=wmts&request=GetTile&version=1.0.0&LAYER=bou&tileMatrixSet=w&TileMatrix={z}&TileRow={y}&TileCol={x}&style=default&format=tiles"
                },
                {
                    overlayer: "http://t{s}.tianditu.cn/DataServer?T=cta_w&X={x}&Y={y}&L={z}"
                }
            ]
        }
    ];