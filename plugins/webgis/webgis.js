//! webGIS 1.0.0 for leaflet 1.0.0
//! China Geology Survey Xi'an ZhanChang WANG(c) 2013-2016 email:970719735@qq.com
//! 注意：未经作者许可，不可对本脚本进行移植、篡改、嵌入等活动！
(function(n, t, i) {
	L.webGIS = "20160821";
	L.iPath = function() {
		for(var u = t.getElementsByTagName("script"), f = /[\/^]webgis[\-\._]?([\w\-\._]*)\.js\??/, r, n = "", i = 0, e = u.length; i < e; i++) r = u[i].src || "", r.match(f) && (n = r.split(f)[0], n = (n ? n + "/" : "") + "images/");
		return n
	}();
	n._ = L.DomUtil.get;
	n.NULL = function(n) {
		return n === i ? !0 : n === null ? !0 : !1
	};
	n.OBJECT = {
		stringify: function(n) {
			var r = function(n) {
					if(n == i) return "";
					if(typeof n == "string") return '"' + n.replace(/([\"\\])/g, "\\$1").replace(/(\n)/g, "\\n").replace(/(\r)/g, "\\r").replace(/(\t)/g, "\\t") + '"';
					if(typeof n == "object") {
						var u, f = [];
						if(n.sort) {
							for(u = 0; u < n.length; u++) f.push(r(n[u]));
							f = "[" + f.join() + "]"
						} else {
							for(u in n) n.hasOwnProperty(u) && f.push('"' + u + '":' + r(n[u]));
							!t.all || /^\n?function\s*toString\(\)\s*\{\n?\s*\[native code\]\n?\s*\}\n?\s*$/.test(n.toString) || f.push("toString:" + n.toString.toString());
							f = "{" + f.join() + "}"
						}
						return f
					}
					return n.toString().replace(/\"\:/g, '":""')
				},
				u = r(n);
			return u === '""' ? "" : u
		},
		parse: function(n) {
			if(typeof n == "string") try {
				return new Function("return " + n)()
			} catch(t) {}
			return n
		},
		toArray: function(n) {
			return n ? (n instanceof Array) ? n : new Array(n) : []
		}
	};
	n.webMercator = L.Projection.SphericalMercator,
		function() {
			var i = {
					supportsFullScreen: !1,
					isFullScreen: function() {
						return !1
					},
					requestFullScreen: function() {},
					cancelFullScreen: function() {},
					fullScreenEventName: "",
					prefix: ""
				},
				u = "webkit moz o ms khtml".split(" "),
				r, f;
			if(typeof t.cancelFullScreen != "undefined") i.supportsFullScreen = !0;
			else
				for(r = 0, f = u.length; r < f; r++)
					if(i.prefix = u[r], typeof t[i.prefix + "CancelFullScreen"] != "undefined") {
						i.supportsFullScreen = !0;
						break
					}
			i.supportsFullScreen && (i.fullScreenEventName = i.prefix + "fullscreenchange", i.isFullScreen = function(n) {
				var i = n ? n.document : t;
				switch(this.prefix) {
					case "":
						return i.fullScreen;
					case "webkit":
						return i.webkitIsFullScreen;
					default:
						return i[this.prefix + "FullScreen"]
				}
			}, i.requestFullScreen = function(n) {
				return this.prefix === "" ? n.requestFullScreen() : n[this.prefix + "RequestFullScreen"]()
			}, i.cancelFullScreen = function(n) {
				var i = n ? n.document : t;
				return this.prefix === "" ? i.cancelFullScreen() : i[this.prefix + "CancelFullScreen"]()
			});
			n.fullScreenApi = i
		}();
	L.DOMtrigger = L.Evented.extend({
		options: {
			draggable: !1,
			map: null,
			propagation: !1
		},
		statics: {
			DOMevents: ["contextmenu", "click", "dblclick", "mouseover", "mouseenter", "mousedown", L.Browser.msPointer ? "MSPointerDown" : L.Browser.pointer ? "pointerdown" : "touchstart", "mousemove", L.Browser.msPointer ? "MSPointerMove" : L.Browser.pointer ? "pointermove" : "touchmove", "mouseup", L.Browser.msPointer ? "MSPointerUp" : L.Browser.pointer ? "pointerup" : "touchend", "mouseout", "mouseleave", "mousewheel", "wheel"]
		},
		initialize: function(n, t) {
			this._icon = n;
			t = t || {};
			L.setOptions(this, t);
			this._mouse = {}
		},
		enable: function() {
			this._enabled || (this._Dom(), this._enabled = !0)
		},
		disable: function() {
			this._enabled && (this._Dom("off"), this._enabled = !1)
		},
		_Dom: function(n) {
			L.DomEvent[n || "on"](this._icon, L.DOMtrigger.DOMevents.join(" "), this._onDOM, this)
		},
		_onDOM: function(n) {
			var s = this.options,
				f = n.touches && n.touches.length === 1 ? n.touches[0] : n,
				u = s.map,
				l = u && s.draggable ? !0 : !1,
				e = n.type,
				r = L.DOMtrigger.DOMevents,
				o = s.propagation,
				h, t, c;
			switch(e) {
				case "contextmenu":
					L.DomEvent.preventDefault(n);
					break;
				case "click":
					if(l && (h = !0, t = this._mouse, t.down && t.move && t.up && t.start && t.end && t.start.distanceTo(t.end) > 2 && (h = !1), (t.down || t.move || t.up) && (this.mouse = {}), !h)) {
						L.DomEvent.stopPropagation(n);
						return
					}
					break;
				default:
					if(l) switch(e) {
						case r[5]:
						case r[6]:
							this._mouse = {
								up: null,
								move: null,
								down: !0,
								start: u.mouseEventToContainerPoint(f),
								end: null
							};
							o = !0;
							break;
						case r[7]:
						case r[8]:
							this._mouse.down && (this._mouse.move = !0);
							o = !0;
							break;
						case r[9]:
						case r[10]:
							this._mouse.down && (this._mouse.up = !0, this._mouse.end = u.mouseEventToContainerPoint(f));
							o = !0
					}
			}
			o !== !0 && L.DomEvent.stopPropagation(n);
			c = L.Util.indexOf(r, e);
			switch(c) {
				case 6:
				case 8:
				case 10:
				case 14:
					e = r[c - 1]
			}
			f.clientX !== i && f.clientY !== i && this.fire(e, {
				originalEvent: n,
				latlng: u ? u.containerPointToLatLng(u.mouseEventToContainerPoint(f)) : null
			})
		}
	});
	L.Util.extend(L.Util, {
		toDOM: function(n) {
			var i, r = t.createElement("div"),
				u = t.createDocumentFragment();
			for(r.innerHTML = n; i = r.firstChild;) u.appendChild(i);
			return u
		},
		rotate: function(n, t) {
			if(n && !isNaN(t))
				if(L.DomUtil.TRANSFORM) n.style[L.DomUtil.TRANSFORM] += " rotate(" + t + "deg)";
				else if(L.Browser.ie) {
				var i = t * (Math.PI / 180),
					r = Math.cos(i),
					u = Math.sin(i);
				n.style.filter += " progid:DXImageTransform.Microsoft.Matrix(sizingMethod='auto expand', M11=" + r + ", M12=" + -u + ", M21=" + u + ", M22=" + r + ")"
			}
		},
		longitude: function(n) {
			var t = 360;
			return n %= t, n + (Math.abs(n) > .5 * t ? (n > 0 ? -1 : 1) * t : 0)
		},
		stringcharleft: function(n, t) {
			var r = [],
				u = 0,
				i, f;
			if(t > 0)
				for(i = 0, f = n.length; i < f; i++)
					if(r.push(n.charAt(i)), u += n.charCodeAt(i) > 255 ? 2 : 1, u >= t) break;
			return r.join("")
		},
		stringcharlength: function(n) {
			for(var i = 0, t = 0, r = n.length; t < r; t++) i += n.charCodeAt(t) > 255 ? 2 : 1;
			return i
		},
		innerhtml2dom: function(n, t) {
			var i = n,
				r = i.cloneNode(!1);
			return r.innerHTML = t, i.parentNode.replaceChild(r, i), r
		},
		StringToLatLngs: function(n, t) {
			for(var r, f = n.split(/[,\s;]+/im), u = Math.floor(f.length / 2), e = [], o = {}, s = {}, i = 0; i < u; i++) {
				if(r = L.latLng({
						lng: this.longitude(f[2 * i]),
						lat: f[2 * i + 1]
					}), u === 1) return r;
				i === 0 && (o = r);
				i === u - 1 && (s = r);
				e.push(r)
			}
			return t && u >= 4 && o.equals(s) && e.pop(), e
		},
		LatLngsToString: function(n, t, i) {
			for(var h, f, e = [], r = {}, o = {}, c = /([+-]?\d*.\d*?)(0*)$/img, s = n.length, u = 0; u < s; u++) h = n[u].wrap(), f = {
				lng: h.lng.toFixed(7).replace(c, "$1"),
				lat: h.lat.toFixed(7).replace(c, "$1")
			}, u === 0 && (r = f), u === s - 1 && (o = f), e.push(f.lng + (t ? "," : " ") + f.lat);
			return i && s > 2 && (r.lng !== o.lng || r.lat !== o.lat) && e.push(r.lng + (t ? "," : " ") + r.lat), e.join(t ? " " : ",")
		},
		Polygons: function(n, t) {
			var i = [],
				u, r, e, f, o, s;
			if(L.Util.isArray(n))
				if(n[0] instanceof L.LatLng) {
					if(i.push(n), L.Util.isArray(t))
						for(u in t) t.hasOwnProperty(u) && L.Util.isArray(t[u]) && t[u][0] instanceof L.LatLng && i.push(t[u])
				} else L.Util.isArray(n[0]) ? i = n : i.push(this.StringToLatLngs(n[0]));
			else i.push(this.StringToLatLngs(n));
			for(r = 0, e = i.length; r < e; r++) f = i[r].length, f >= 2 && (o = i[r][0], s = i[r][f - 1], o.equals(s) && i[r].pop());
			return i
		},
		deg2dms: function(n, t) {
			NULL(n) && (n = 0);
			NULL(t) && (t = 2);
			var f = Math.abs(n),
				r = Math.floor(f).toFixed(0),
				i = Math.floor((f - r) * 60).toFixed(0),
				e = f * 3600 - r * 3600 - i * 60,
				u = ((t >= 0 ? e.toFixed(t) : e) + "").replace(/(^[+-]?\d*.\d*?)(0*)$/img, "$1").replace(/(^[+-]?\d*)((.?)|(.\d+))$/img, "$1$4");
			return u >= 60 && (i++, u -= 60), i >= 60 && (r++, i -= 60), (n < 0 ? "-" : "") + r + "°" + (i > 0 ? i + "′" + (u > 0 ? u + "″" : "") : "")
		},
		dms2deg: function(n) {
			var t, r = 0,
				u = 0,
				i = n.replace(/^([\-+]?[0-9]{1,3})[°|度]{1}(([0-9]{1,2})[′|'|分]{1})?(([0-9]{1,2}(\.[0-9]+)?)[″|"|秒]{1})?$/img, "$1,$3,$5").split(",");
			return t = parseInt(i[0]), Math.abs(t) > 180 && (i = n.replace(/^([\-+]?[0-9]{1,3})([0-9]{2})([0-9]{2}(\.[0-9]+)?)$/img, "$1,$2,$3").split(","), t = parseInt(i[0])), i.length === 3 ? (r = i[1] / 60, u = i[2] / 3600) : t = i[0], t >= 0 ? t + r + u : t - r - u
		},
		center6: function(n) {
			return n >= 0 ? 6 * (Math.floor(n / 6) + 1) - 3 : -1 * (6 * (Math.floor(Math.abs(n) / 6) + 1) - 3)
		},
		center3: function(n) {
			return n > 0 ? n >= 1.5 ? (Math.floor((n - 1.5) / 3) + 1) * 3 : 0 : n < 0 ? -3 * (Math.floor((Math.abs(n) - 1.5) / 3) + 1) : 0
		},
		zone6: function(n) {
			return n > 0 ? Math.floor(n / 6) + 1 : n < 0 ? Math.floor((n + 180) / 6) + 1 : ""
		},
		zone3: function(n) {
			return n > 0 ? n >= 1.5 ? Math.floor((n - 1.5) / 3) + 1 : "" : n < 0 ? Math.floor((n + 180 - 1.5) / 3) + 1 : ""
		},
		gauss: function(n, t, i, r) {
			var e;
			NULL(r) && (r = 54);
			e = parseInt(r) === 54;
			t = t * Math.PI / 180;
			n = (n - i) * Math.PI / 180;
			var s = (.999999999477 * t + (e ? .00334672241787 : .00334720411189) * Math.pow(t, 3) - (e ? .000652614410707 : .00065270594719) * Math.pow(t, 5) + (e ? 56031408623e-15 : 560384115892e-16) * Math.pow(t, 7) - (e ? 197720665939e-17 : 197731473575e-17) * Math.pow(t, 9)) * (e ? 6335552.71700043 : 6335442.27525873),
				u = Math.pow(Math.tan(t), 2),
				f = (e ? .00673852541468 : .006739501819473) * Math.pow(Math.cos(t), 2),
				o = (e ? 6399698.90178271 : 6399596.65198801) / Math.sqrt(1 + f);
			return {
				x: s + (1 + Math.pow(Math.cos(t) * n, 2) * (5 - u + 9 * f + 4 * f * f) / 12 + Math.pow(Math.cos(t) * n, 4) * (61 - 58 * u + u * u + 270 * f - 330 * f * u) / 360) * Math.sin(2 * t) * n * n * .25 * o,
				y: 5e5 + (1 + Math.pow(n * Math.cos(t), 2) * (1 - u + f) / 6 + Math.pow(n * Math.cos(t), 4) * (5 - 18 * u + u * u + 14 * f - 58 * f * u) / 120) * Math.cos(t) * n * o
			}
		},
		samezone: function(n, t) {
			var i = this.center6(n),
				r = this.center6(t),
				u = !1;
			return i === r ? u = !0 : Math.abs(i - r) === 6 && (i < r ? (r - 3) * 1e6 === Math.floor(t * 1e6) && (r = i, u = !0) : (i - 3) * 1e6 === Math.floor(n * 1e6) && (i = r, u = !0)), {
				same: u,
				center1: i,
				center2: r
			}
		},
		distance: function(n, t, i) {
			var r, u, f;
			if(NULL(i) && (i = 54), n = L.latLng(n.lat, this.longitude(n.lng)).wrap(), t = L.latLng(t.lat, this.longitude(t.lng)).wrap(), r = this.samezone(n.lng, t.lng), r.same) switch(i) {
				case 54:
				case 80:
					return n = this.gauss(n.lng, n.lat, r.center1, i), t = this.gauss(t.lng, t.lat, r.center2, i), Math.sqrt(Math.pow(t.y - n.y, 2) + Math.pow(t.x - n.x, 2))
			}
			return u = webMercator.project(n), f = webMercator.project(t), Math.sqrt(Math.pow(f.y - u.y, 2) + Math.pow(f.x - u.x, 2))
		},
		area: function(n, t, i) {
			var l, a, u, v, y, s, h, f, e, p;
			t = NULL(t) ? 54 : parseInt(t);
			n = this.Polygons(n, i);
			for(var o = [], b = n.length, w = L.latLngBounds(n[0]), c = this.samezone(w.getSouthWest().lng, w.getNorthEast().lng), r = 0; r < b; r++) {
				for(l = n[r].length, a = 0, u = 0, e = 0; e < l; e++) u++, u === l && (u = 0), c.same && (t === 54 || t === 80) ? (v = n[r][e], y = n[r][u], s = this.gauss(v.lng, v.lat, c.center1, t), h = this.gauss(y.lng, y.lat, c.center2, t)) : (f = webMercator.project(n[r][e]), s = {
					x: f.x,
					y: f.y
				}, f = webMercator.project(n[r][u]), h = {
					x: f.x,
					y: f.y
				}), a += (h.y - s.y) * (h.x + s.x);
				o.push(Math.abs(a * .5))
			}
			for(p = o[0], r = 1; r < o.length; r++) p -= o[r];
			return p
		},
		perimeter: function(n, t) {
			var r = 0,
				u, i;
			if(L.Util.isArray(n))
				for(L.Util.isArray(n[0]) && (n = n[0], n.push(n[0])), u = n.length, i = 1; i < u; i++) r += this.distance(n[i], n[i - 1], t);
			return r
		},
		contains: function(n, t, i) {
			var s = !1,
				c, e, u;
			if(t = this.Polygons(t, i), t)
				for(c = t.length, e = 0; e < c; e++) {
					var r = t[e],
						l = r.length,
						o = !1,
						h, f = 0;
					for(u = 0; u < l; u++) f++, f === l && (f = 0), (r[u].lat < n.lat && r[f].lat >= n.lat || r[f].lat < n.lat && r[u].lat >= n.lat) && (h = r[f].lat - r[u].lat, Math.abs(h) > 1e-15 && r[u].lng + (n.lat - r[u].lat) * (r[f].lng - r[u].lng) / h < n.lng && (o = !o));
					if(e === 0)
						if(o) s = !0;
						else break;
					else if(o) {
						s = !1;
						break
					}
				}
			return s
		},
		checkcross: function(n, t) {
			var y, c, r, f, s, l, u, e, h, a, i, o, p, v;
			if(L.Util.isArray(t)) {
				for(L.Util.isArray(t[0]) && (t = t[0]), y = t.length, c = 0; c < y; c++)
					if(this.contains(t[c], n)) return !0;
				for(c = 0; c < y - 1; c++)
					for(r = t[c].lng, f = t[c].lat, s = t[c + 1].lng, l = t[c + 1].lat, p = n.length, v = 0; v < p - 1; v++)
						if(u = n[v].lng, e = n[v].lat, h = n[v + 1].lng, a = n[v + 1].lat, Math.abs(h - u) < 1e-15) {
							if(i = u, Math.abs(s - r) > 1e-15 && i > Math.min(r, s) && i < Math.max(r, s) && (o = f + (l - f) * (i - r) / (s - r), o > Math.min(e, a) && o < Math.max(e, a))) return !0
						} else if(Math.abs(a - e) < 1e-15) {
					if(o = e, Math.abs(l - f) > 1e-15 && (i = r + (o - f) * (s - r) / (l - f), o > Math.min(f, l) && o < Math.max(f, l) && i > Math.min(u, h) && i < Math.max(u, h))) return !0
				} else if(Math.abs(l - f) < 1e-15) {
					if(o = f, i = u + (o - e) * (h - u) / (a - e), o > Math.min(e, a) && o < Math.max(e, a) && i > Math.min(r, s) && i < Math.max(r, s)) return !0
				} else if(Math.abs(s - r) < 1e-15) {
					if(i = r, o = e + (a - e) * (i - u) / (h - u), o > Math.min(f, l) && o < Math.max(f, l) && i > Math.min(u, h) && i < Math.max(u, h)) return !0
				} else if(i = (e - f - u * (a - e) / (h - u) + r * (l - f) / (s - r)) / ((l - f) / (s - r) - (a - e) / (h - u)), i > Math.min(r, s) && i < Math.max(r, s) && i > Math.min(u, h) && i < Math.max(u, h)) return !0;
				return !1
			}
			return t instanceof L.LatLng ? this.contains(t, n) : null
		},
		containspoly: function(n, t) {
			var r, i;
			if(n = this.Polygons(n), L.Util.isArray(t)) {
				for(L.Util.isArray(t[0]) && (t = t[0]), r = t.length, i = 0; i < r; i++)
					if(!this.contains(t[i], n)) return !1;
				return !0
			}
			return t instanceof L.LatLng ? this.contains(t, n) : null
		},
		sheet: function(n, t) {
			function s(n, t) {
				var o = L.Util.isArray(n),
					s = n.hasOwnProperty("name"),
					u, f, e;
				if(o || s) {
					if(i.push(l(t)), r.push(a), o)
						for(u = 0, f = n.length; u < f; u++) e = n[u], NULL(e) || h(e, f > 1 ? u < f - 1 ? 1 : 2 : null);
					else h(n);
					i.push(r.pop())
				} else NULL(n) || i.push(c(n.valueOf()))
			}

			function h(n, t) {
				n.hasOwnProperty("name") && n.hasOwnProperty("content") ? (i.push(f), r.push(e), i.push(y(n.alias ? n.alias : "", t) + n.name + u), i.push(p(t)), r.push(u), s(n.content, 1), i.push(r.pop()), i.push(r.pop())) : i.push(f + v(t) + c(n.name ? n.name : n.content ? n.content : n.valueOf()) + u + e)
			}

			function c(n) {
				return typeof n == "string" ? n.replace(/((((&)|(\\u0026))(amp;)+)|(\\u0026))/img, "&").replace(/((&lt;)|(\\u003c))([\s\S]*?)((&gt;)|(\\u003e))/img, "<$4>").replace(/(&((#34;)|(quot;)))/img, '"').replace(/(&((#34;)|(apos;)))/img, "'") : n
			}
			var l = function(n) {
					return "<table " + (NULL(t) && NULL(n) ? 'class="xmlstyle" ' : "") + 'style="width:100%" bgcolor="White" cellpadding="0" cellspacing="0">'
				},
				a = "<\/table>",
				f = "<tr>",
				e = "<\/tr>",
				v = function(n) {
					return '<td class="xmlstyle' + (NULL(n) ? "" : n === 1 ? "rt" : "rb") + '" style="width:100%;" colspan="2">'
				},
				y = function(n, t) {
					return '<td class="xmlstyle' + (NULL(t) ? "" : t === 1 ? "lt" : "lb") + '"' + (n && n.length > 0 ? ' title="' + n + '"' : "") + ' style="white-space:nowrap;text-align:right">'
				},
				p = function(n) {
					return '<td class="xmlstyle' + (NULL(n) ? "" : n === 1 ? "rt" : "rb") + '" style="width:100%;">'
				},
				u = "<\/td>",
				r = [],
				i = [],
				o = OBJECT.parse(n);
			return s(NULL(o) ? null : o), i.join("")
		},
		dig: function(n, t, i) {
			function f(n, t, i) {
				if(L.Util.isArray(n))
					for(var r = 0; r < n.length; r++) n[r].hasOwnProperty("name") && n[r].hasOwnProperty("content") ? f(n[r].content, t, n[r].name === t ? !0 : i) : f(n[r], t, i);
				else isNaN(t) ? n.hasOwnProperty("name") && n.hasOwnProperty("content") ? f(n.content, t, n.name === t ? !0 : i) : i === !0 && e.push(n) : n.hasOwnProperty("name") && n.hasOwnProperty("content") ? f(n.content, t, i) : e.push(n)
			}
			var u = OBJECT.parse(n),
				e, s, o, h, r;
			if(NULL(u)) return null;
			for(e = [], L.Util.isArray(u) || (u = OBJECT.toArray(u)), s = ("" + t).split(/[,;: ]+/), o = 0, h = s.length; o < h; o++) r = L.Util.trim("" + s[o]), r.length > 0 && (isNaN(r) ? f(u, r) : (r = parseInt(r), r >= 0 && r < u.length && f(u[r], r, !0)));
			return e.length > 0 ? e.join(i ? i : "<hr/>") : null
		},
		getElementPos: function(n) {
			var t, i, r;
			if(n = _(n), t = new L.Point, t.x = t.y = t.width = t.height = 0, n.offsetParent)
				for(t.x = n.offsetLeft, t.y = n.offsetTop, i = n.offsetParent; i;) t.x += i.offsetLeft, t.y += i.offsetTop, r = i.tagName.toLowerCase(), r !== "table" && r !== "body" && r !== "html" && r !== "div" && i.clientTop && i.clientLeft && (t.x += i.clientLeft, t.y += i.clientTop), i = i.offsetParent;
			else n.left && n.top ? (t.x = n.left, t.y = n.top) : (n.x && (t.x = n.x), n.y && (t.y = n.y));
			return n.offsetWidth && n.offsetHeight ? (t.width = n.offsetWidth, t.height = n.offsetHeight) : n.style && n.style.pixelWidth && n.style.pixelHeight && (t.width = n.style.pixelWidth, t.height = n.style.pixelHeight), t
		}
	});
	L.Symbol = L.Marker.extend({
		initialize: function(n, t) {
			if(L.Marker.prototype.initialize.call(this, n, t), t = this.options, !NULL(t.rank) && t.rank <= 3 && t.interactive) {
				this.options.nonBubblingEvents = [];
				var r = t.domEvents;
				this.domEvents = r !== i ? r : L.DOMtrigger.DOMevents
			}
		},
		_initInteraction: function() {
			var n = this.options,
				t;
			n.interactive && (L.DomUtil.addClass(this._icon, "leaflet-interactive"), t = NULL(n.rank) || n.rank > 3, t ? this.addInteractiveTarget(this._icon) : (this.removeInteractiveTarget = function() {}, this.DOMtrigger = new L.DOMtrigger(this._icon, {
				map: this._map,
				draggable: n.draggable,
				propagation: n.propagation
			}), this.DOMtrigger.enable(), this._domEvent()), n.draggable && t && (this.dragging = new L.Handler.MarkerDrag(this), this.dragging.enable()))
		},
		_domEvent: function(n) {
			var i = this.domEvents,
				t;
			i !== null && (t = OBJECT.toArray(i), t.length > 0 && this.DOMtrigger[n || "on"](t.join(" "), this._domFire, this))
		},
		_domFire: function(n) {
			this.fire(n.type, n)
		},
		_setPos: function(n) {
			var t = this.options;
			L.Marker.prototype._setPos.call(this, n);
			L.Util.rotate(this._icon, t.angle);
			L.Util.rotate(this._shadow, t.angle)
		},
		bringToFront: function() {
			var t = this._shadow,
				n;
			return t && L.DomUtil.toFront(t), n = this._icon, n && L.DomUtil.toFront(n), this._bindHtmlObject && this._bindHtmlObject.bringToFront(), this
		},
		bringToBack: function() {
			var n, t;
			return this._bindHtmlObject && this._bindHtmlObject.bringToBack(), n = this._icon, n && L.DomUtil.toBack(n), t = this._shadow, t && L.DomUtil.toBack(t), this
		},
		_bringToFront: function() {
			return this.bringToFront()
		},
		_resetZIndex: function() {
			return this.bringToBack()
		},
		onRemove: function(n) {
			this.DOMtrigger && (this._domEvent("off"), this.DOMtrigger.disable());
			L.Marker.prototype.onRemove.call(this, n)
		}
	});
	L.symbol = function(n, t) {
		return new L.Symbol(n, t)
	};
	L._Symbol = L.Class.extend({
		options: {
			markerno: 0,
			markertype: 0,
			outline: !1,
			select: !1,
			shadow: !1,
			iconSize: [20, 20],
			iconAnchor: [10, 10],
			shadowSize: [31, 31],
			shadowAnchor: [16, 16],
			className: ""
		},
		initialize: function(n) {
			L.setOptions(this, n)
		},
		createIcon: function(n) {
			return this._createIcon("icon", n)
		},
		createShadow: function(n) {
			return this.options.shadow ? this._createIcon("shadow", n) : null
		},
		getMarker: function() {
			return this.options.markerno
		},
		getMarkerType: function() {
			return this.options.markertype
		},
		setStyle: function(n) {
			var t = this.options;
			n = n || {};
			NULL(n.select) || (t.select = n.select);
			NULL(n.markertype) || (t.markertype = n.markertype);
			NULL(n.markerno) || (t.markerno = n.markerno);
			this._img.src = this._url(!1);
			t.outline && (this._img.style.outline = t.select ? "#ffff00 dashed 1px" : "none")
		},
		_url: function(n) {
			var i = this.options,
				r = i.markerno,
				t;
			switch(parseInt(i.markertype)) {
				case 0:
					t = ".gif";
					break;
				case 1:
					t = ".png";
					break;
				case 2:
					t = ".jpg";
					break;
				case 3:
					t = ".bmp";
					break;
				default:
					t = ""
			}
			return isNaN(r) ? r : L.iPath + "marker/marker" + r + (n ? "d" : i.select ? "s" : "") + t
		},
		_createIcon: function(n, i) {
			var r = !i || i.tagName !== "IMG" ? null : i,
				o;
			r = r || t.createElement("img");
			r.src = this._url(n === "shadow" ? !0 : !1);
			var u = this.options,
				f = L.point(u[n + "Size"]),
				e = n === "shadow" ? L.point(u.shadowAnchor || u.iconAnchor) : L.point(u.iconAnchor);
			return !e && f && (e = f.divideBy(2, !0)), r.className = "leaflet-marker-" + n + " " + u.className, o = r.style, e && (o.marginLeft = -e.x + "px", o.marginTop = -e.y + "px"), f && (o.width = f.x + "px", o.height = f.y + "px"), this._img = r
		}
	});
	L.Polygon.prototype.getCenter = function(n) {
		function lt(n, t) {
			return n - t
		}
		var u = L.Util.Polygons(n || this.getLatLngs(), this._holes),
			ut, ft, f, e, i, r, o, d, g, nt, w, et, c, rt, b, ht, t, v, y, p, k, ct;
		if(u.length === 0) return null;
		for(ut = u[0].length, ft = ut - 1, i = r = d = 0, g = 0; g < ut; ft = g++) f = u[0][g], e = u[0][ft], o = f.lng * e.lat - e.lng * f.lat, i += (f.lng + e.lng) * o, r += (f.lat + e.lat) * o, d += f.lng * e.lat, d -= f.lat * e.lng;
		o = 3 * d;
		i /= o;
		r /= o;
		var s = [],
			h = [],
			at = u.length;
		for(nt = 0; nt < at; nt++)
			for(w = u[nt], et = w.length, c = 0; c < et; c++) {
				var ot = w[c],
					l = ot.lng,
					a = ot.lat,
					st = c + 1 === et ? w[0] : w[c + 1],
					tt = st.lng,
					it = st.lat;
				(l < i && tt > i || l > i && tt < i) && s.push(a + (i - l) * (it - a) / (tt - l));
				(a < r && it > r || a > r && it < r) && h.push(l + (r - a) * (tt - l) / (it - a))
			}
		if(rt = null, s.length % 2 == 0 && h.length % 2 == 0) {
			for(s.sort(lt), h.sort(lt), b = 0, t = 0; t < s.length / 2; t++) v = s[t * 2], y = s[t * 2 + 1], p = Math.abs(v - y), p >= b && (b = p, ht = .5 * (v + y));
			for(k = 0, t = 0; t < h.length / 2; t++) v = h[t * 2], y = h[t * 2 + 1], p = Math.abs(v - y), p >= k && (k = p, ct = .5 * (v + y));
			rt = L.latLng(b > k ? ht : r, b > k ? i : ct)
		} else rt = u[0][0];
		return rt
	};
	L.Polyline.prototype.getCenter = function(n) {
		n = n || this.getLatLngs();
		for(var e, u, f, i, o, s = n.length, t = 0, r = 0; t < s - 1; t++) r += n[t].distanceTo(n[t + 1]) / 2;
		if(r < 1e-15) return n[0];
		for(t = 0, u = 0; t < s - 1; t++)
			if(f = n[t], i = n[t + 1], e = f.distanceTo(i), u += e, u > r) return o = (u - r) / e, L.latLng(i.lat - o * (i.lat - f.lat), i.lng - o * (i.lng - f.lng));
		return n[0]
	};
	L.TileLayer.prototype.getTileUrl = function(n) {
		function r(n, t, i) {
			for(var r, f, e = [], u = i; u > 0; u--) r = 0, f = 1 << u - 1, (n & f) != 0 && r++, (t & f) != 0 && (r += 2), e.push(r);
			return e.join("")
		}

		function u(n, t, i) {
			var r = function(n, t, i) {
				for(var r = n.toString(t), u = r.length; u < i;) r = "0" + r, u++;
				return r
			};
			return "L" + r(i, 10, 2) + "/R" + r(t, 16, 8) + "/C" + r(n, 16, 8)
		}
		var t = {
				r: L.Browser.retina ? "@2x" : "",
				s: this._getSubdomain(n),
				x: n.x,
				y: n.y,
				z: this._getZoomForUrl()
			},
			i;
		return this._url.match(/\{m\}/mg) ? t.m = r(t.x, t.y, t.z) : this._url.match(/\{e\}/mg) ? t.e = u(t.x, t.y, t.z) : (this._url.match(/\{([xyz]{1})\}[\s\S]*\{([xyz]{1})\}[\s\S]*\{([xyz]{1})\}/mg) || (this._url.match(/\/$/mg) || (this._url += "/"), this._url += "{z}/{x}/{y}.png"), this._map && !this._map.options.crs.infinite && (i = this._globalTileRange.max.y - n.y, this.options.tms && (t.y = i), t["-y"] = i)), L.Util.template(this._url, L.extend(t, this.options))
	};
	L.GroundOverlay = L.ImageOverlay.extend({
		options: {
			interactive: !0
		},
		initialize: function(n, t, i) {
			L.setOptions(this, i);
			L.ImageOverlay.prototype.initialize.call(this, n, t, this.options)
		},
		onAdd: function() {
			L.ImageOverlay.prototype.onAdd.call(this);
			this._icon = this._image;
			this.setStyle()
		},
		_angle: function() {
			var n = this.options.angle;
			if(!isNaN(n))
				if(L.DomUtil.TRANSFORM) this._image.style[L.DomUtil.TRANSFORM] += " rotate(" + n + "deg)";
				else if(L.Browser.ie) {
				var t = n * (Math.PI / 180),
					i = Math.cos(t),
					r = Math.sin(t);
				this._image.style.filter += " progid:DXImageTransform.Microsoft.Matrix(sizingMethod='auto expand', M11=" + i + ", M12=" + -r + ", M21=" + r + ", M22=" + i + ")"
			}
		},
		_reset: function() {
			L.ImageOverlay.prototype._reset.call(this);
			this._angle()
		},
		_animateZoom: function(n) {
			L.ImageOverlay.prototype._animateZoom.call(this, n);
			this._angle()
		},
		getLatLng: this.getBounds,
		setLatLng: this.setBounds,
		getCenter: function() {
			return this._bounds.getCenter()
		},
		setStyle: function(n) {
			var i = this._image,
				t;
			i && (n = n || {}, t = this.options, NULL(n.select) || (t.select = n.select), i.style.outline = t.select ? "#ffff00 dashed 1px" : "none")
		}
	});
	L.groundOverlay = function(n, t, i) {
		return new L.GroundOverlay(n, t, i)
	};
	L.Html = L.Layer.extend({
		options: {
			followmap: !0,
			htmlstyle: "border-radius:3px;box-shadow:0 1px 3px rgba(0,0,0,0.45);border:1px solid #999;padding:0px 3px 0px 3px;background-color:#ffffff;white-space:nowrap;vertical-align:middle;text-align:center;width:100%;height:100%;",
			interactive: !1,
			propagation: !1,
			draggable: !1,
			clickclose: !0,
			type: null,
			align: "lt",
			dx: 0,
			dy: 0,
			animated: !0,
			pane: "shadowPane",
			opacity: 1,
			fadeable: !0,
			fadetime: 300,
			fadestep: .05,
			age: 0
		},
		initialize: function(n, t, r) {
			this._latlng = n ? L.latLng(n) : null;
			this._html = t ? t : null;
			L.setOptions(this, r);
			var u = this.options.domEvents;
			this.domEvents = u !== i ? u : L.DOMtrigger.DOMevents;
			this._firstShow = !0
		},
		onAdd: function(n) {
			this._map = n;
			var t = this.options;
			return this._animated = t.animated, this._icon = L.DomUtil.create("div", t.followmap ? "leaflet-zoom-" + (this._animated ? "animated" : "hide") : "leaflet-control"), this._icon.style.position = "absolute", this._container = t.followmap ? this._map._panes.hasOwnProperty(t.pane) ? this._map._panes[t.pane] : this._map._panes.shadowPane : this._map.getContainer(), this._icon.title = typeof t.title == "string" ? t.title : "", this.show(!1), this._container.appendChild(this._icon), this._events(), this.fire("add", {
				layer: this
			}), this.setHtmlContent(this._html), this.setHtmlOption(t, !0, !0), this
		},
		remove: function(n) {
			var t = this._map;
			t && (n === !0 ? t.removeLayer(this) : this._fade.call(this, !1, function() {
				t.removeLayer(this);
				typeof n == "function" && n.call(this)
			}))
		},
		onRemove: function() {
			if(this.stayhandle && clearTimeout(this.stayhandle), this.fadehandle && clearInterval(this.fadehandle), this._events("off"), this._icon) {
				var n = this._icon.parentNode;
				n && n.removeChild(this._icon)
			}
			this._icon = null;
			this.fire("remove")
		},
		setHtmlLatLng: function(n) {
			var t, i;
			if(n)
				if(this._firstShow) this._latlng = L.latLng(n), this._reset();
				else if(this.fire("move", {
					latlng: this._latlng
				}), t = this._latlng = L.latLng(n), this._animated) {
				i = new L.PosAnimation;
				i.on({
					end: function() {
						this.fire("moveend", {
							latlng: t
						})
					}
				}, this);
				this._map && this._icon && (this._canshow() ? (this.show(!0), i.run(this._icon, this._lefttop(this._map.latLngToLayerPoint(t)))) : this.show(!1))
			} else this._setPos() && this.fire("moveend", {
				latlng: t
			});
			return this
		},
		getHtmlLatLng: function() {
			return this._latlng
		},
		setHtmlContent: function(n, t) {
			var i, r;
			return n && (this._html = n, i = this._icon, i && (typeof n == "string" ? (r = this.options.htmlstyle, i.innerHTML = r ? '<div style="' + r + '">' + n + "<\/div>" : n) : i.appendChild(n), t === !0 && this._reset())), this
		},
		getHtmlContent: function() {
			return this._html
		},
		setHtmlOpacity: function(n) {
			if(this._icon) {
				var t = this._icon.style;
				n = parseFloat(n);
				n >= 0 && n < 1 ? (t.filter = "alpha(opacity=" + n * 100 + ")", t.opacity = n) : t.filter = t.opacity = ""
			}
			return this
		},
		setHtmlOption: function(n, t, r) {
			var u, e, f, o, s, h, a, c, l;
			return t !== !0 && L.setOptions(this, n), u = this.options, /(^[lcr][tmb]$)|(^[tmb][lcr]$)/im.test(u.align) || (u.align = "lt"), e = this._icon, e && (f = e.style, o = u.width, o !== i && (f.width = o !== null ? o + "px" : ""), s = u.height, s !== i && (f.height = s !== null ? s + "px" : ""), this.setHtmlOpacity(u.opacity), h = u.border, typeof h == "string" ? f.border = h : h == null && (f.border = ""), a = u.howfloat, typeof a == "string" && (f.styleFloat = f.cssFloat = a), c = u.color, typeof c == "string" ? f.backgroundColor = c : c == null && (f.backgroundColor = ""), l = u.img, l ? (f.backgroundImage = "url('" + l + "')", u.repeat && (f.backgroundRepeat = u.repeat)) : l == null && (f.backgroundImage = ""), e.title = typeof u.title == "string" ? u.title : ""), r === !0 && this._reset(), this
		},
		show: function(n) {
			var i = this._icon,
				r = this.options.angle,
				t = i.style;
			if(t.visibility.toLowerCase().indexOf("hidden") > -1) {
				if(NULL(n)) return !1;
				n && (L.Util.rotate(i, r), t.visibility = "", this.fire("show"))
			} else {
				if(NULL(n)) return !0;
				n ? L.Util.rotate(i, r) : (t.visibility = "hidden", this.fire("hide"))
			}
			return t.visibility
		},
		_reset: function() {
			this._setPos() && (this._firstShow = !1, this._fade(!0, function() {
				this._stayer()
			}))
		},
		_fade: function(n, t) {
			var r, f, u, i;
			this._icon && (r = +this.options.opacity, this.options.fadeable ? (this.stayhandle && clearTimeout(this.stayhandle), this.fadehandle && clearInterval(this.fadehandle), n || (n = !1), f = r === 0 ? 0 : this.options.fadetime * this.options.fadestep / r, u = 0, this.setHtmlOpacity(n ? u : r - u), i = this, i.fadehandle = setInterval(L.bind(function() {
				u <= r ? (i.setHtmlOpacity(n ? u : r - u), f > 0 && (u += i.options.fadestep)) : (i.fadehandle && clearInterval(i.fadehandle), i.fadehandle = null, i.setHtmlOpacity(n ? r : 0), typeof t == "function" && t.call(i))
			}, i), f)) : (this.setHtmlOpacity(r), typeof t == "function" && t.call(this)))
		},
		_stayer: function() {
			this._icon && this.options.age > 0 && (this.stayhandle = setTimeout(L.bind(this.remove, this), this.options.age))
		},
		_canshow: function() {
			var u = this._map,
				n = this.options,
				t = n.minzoom,
				i = n.maxzoom,
				r = u.getZoom();
			return(NULL(t) || +r >= +t) && (NULL(i) || +r <= +i)
		},
		_setPos: function(n) {
			var r = this._map,
				e = this.options,
				f = !1,
				u, t, i;
			if(r) {
				if(this._canshow()) {
					if(e.followmap) this._latlng && (u = n ? r._latLngToNewLayerPoint(this._latlng, n.zoom, n.center).round() : r.latLngToLayerPoint(this._latlng));
					else {
						t = r.getSize().x;
						i = r.getSize().y;
						switch(e.align.toLowerCase()) {
							case "lt":
							case "tl":
								t = i = 0;
								break;
							case "ct":
							case "tc":
								t /= 2;
								i = 0;
								break;
							case "rt":
							case "tr":
								i = 0;
								break;
							case "lm":
							case "ml":
								t = 0;
								i /= 2;
								break;
							case "cm":
							case "mc":
								t /= 2;
								i /= 2;
								break;
							case "rm":
							case "mr":
								i /= 2;
								break;
							case "lb":
							case "bl":
								t = 0;
								break;
							case "cb":
							case "bc":
								t /= 2
						}
						u = L.point(t, i)
					}
					u && (L.DomUtil.setPosition(this._icon, this._lefttop(u)), f = !0)
				}
				this.show(f)
			}
			return f
		},
		_onViewReset: function() {
			this._setPos()
		},
		_domEvent: function(n) {
			var t = this.domEvents,
				i;
			t !== null && (t = OBJECT.toArray(t), i = "mouseout", t.hasOwnProperty(i) || t.push(i), this.DOMtrigger[n || "on"](t.join(" "), this._domFire, this))
		},
		_domFire: function(n) {
			this.fire(n.type, n)
		},
		_events: function(n) {
			n = n || "on";
			var t = this.options,
				i = this._map;
			if(i[n]({
					resize: this._onViewReset,
					viewreset: this._onViewReset
				}, this), this._animated && i[n]("zoomanim", this._setPos, this), t.interactive && (n === "on" ? (this.DOMtrigger = new L.DOMtrigger(this._icon, {
					map: this._map,
					draggable: t.draggable,
					propagation: t.propagation
				}), this.DOMtrigger.enable(), this._domEvent()) : (this._domEvent("off"), this.DOMtrigger.disable(), this.DOMtrigger = null), t.draggable))
				if(this[n]("mouseout", this._onMouseout, this), n === "on") {
					this.dragging || (this.dragging = new L.Draggable(this._icon));
					this.dragging.on(this.DragEvent = {
						dragstart: this._dragStart,
						drag: this._drag,
						dragend: this._dragEnd
					}, this);
					this.dragging.enable()
				} else this.dragging && (this.dragging.off(this.DragEvent, this), this.dragging.disable(), this.dragging = null)
		},
		_onMouseout: function(n) {
			var t = this.dragging;
			t && t._moving && t._onUp(n)
		},
		_dragStart: function(n) {
			this.options.followmap || (this.iconlefttop = n.target._startPoint);
			this.fire("htmlMoveStart")
		},
		_drag: function() {
			this.show(!0)
		},
		_dragEnd: function(n) {
			var t = this.options,
				r = this._map,
				i;
			t.followmap ? this._latlng = r.layerPointToLatLng(this._lefttop(r.latLngToLayerPoint(this._latlng), !0)) : (i = n.target._newPos.subtract(n.target._startPos).add(n.target._startPoint), t.dx += i.x - this.iconlefttop.x, t.dy += i.y - this.iconlefttop.y);
			this.fire("htmlMoveEnd", t.followmap ? {
				latlng: this._latlng
			} : {
				dx: t.dx,
				dy: t.dy
			})
		},
		_lefttop: function(n, t) {
			var e = this._icon,
				r = this.options;
			r.width == null && e.offsetWidth > 0 && (e.style.width = (r.width = e.offsetWidth) + "px");
			r.height == null && e.offsetHeight > 0 && (e.style.height = (r.height = e.offsetHeight) + "px");
			var i = n[t ? "subtract" : "add"](L.point(r.dx, r.dy)),
				u = r.width,
				f = r.height;
			if(!(isNaN(u) || isNaN(f))) {
				u = (t ? -1 : 1) * u;
				f = (t ? -1 : 1) * f;
				switch(r.align.toLowerCase()) {
					case "mc":
					case "cm":
						i.x -= u / 2;
						i.y -= f / 2;
						break;
					case "tc":
					case "ct":
						i.x -= u / 2;
						break;
					case "bc":
					case "cb":
						i.x -= u / 2;
						i.y -= f;
						break;
					case "ml":
					case "lm":
						i.y -= f / 2;
						break;
					case "mr":
					case "rm":
						i.x -= u;
						i.y -= f / 2;
						break;
					case "bl":
					case "lb":
						i.y -= f;
						break;
					case "tr":
					case "rt":
						i.x -= u;
						break;
					case "br":
					case "rb":
						i.x -= u;
						i.y -= f
				}
			}
			return i
		},
		bringToFront: function() {
			return this._updateZIndex(!0)
		},
		bringToBack: function() {
			return this._updateZIndex(!1)
		},
		_bringToFront: function() {
			return this.bringToFront()
		},
		_resetZIndex: function() {
			return this.bringToBack()
		},
		_updateZIndex: function(n) {
			if(this.options.followmap) {
				var t = this._icon;
				t && (n ? L.DomUtil.toFront(t) : L.DomUtil.toBack(t))
			}
			return this
		}
	});
	L.html = function(n, t, i) {
		return new L.Html(n, t, i)
	};
	L.Bubble = L.Html.extend({
		options: {
			edit: !1,
			type: null,
			closeButton: !0,
			autoPan: !0,
			autoPanPadding: [5, 5],
			animated: !0
		},
		statics: {
			THIS: null
		},
		initialize: function(n, t) {
			this._New = !0;
			L.Bubble.THIS && (L.Bubble.THIS._onexit() || (L.Bubble.THIS._adjustPan(), this._New = !1));
			this._New && (L.Bubble.THIS = this, n = n || {}, n.htmlstyle = null, n.interactive = !1, n.align = "cb", n.dx = 46 + (n.dx ? n.dx : 0), n.dy = 0 + (n.dy ? n.dy : 0), n.pane = "popupPane", n.followmap = !0, n.fadeable = !0, L.setOptions(this, n), this.options.edit && (this.options.age = 0, this.options.closeButton = !0), L.Html.prototype.initialize.call(this, null, null, this.options), this._bindsource = t, this._divEvents = !1, this.id = "Bubble_" + L.stamp(this), this.set = {
				items: null,
				pointer: null,
				pages: null,
				page: null,
				isdirty: null,
				MENUid: null,
				HTMLid: null,
				BACKWARDid: null,
				FORWARDid: null,
				FIELDNAMEid: null,
				FIELDALIASid: null,
				FIELDSUBMITid: null,
				FIELDADDbeforeid: null,
				FIELDADDafterid: null,
				FIELDDELETEid: null,
				FIELDNEWid: null,
				ISarray: null
			})
		},
		addTo: function(n) {
			return this._New && n.addLayer(this), this
		},
		remove: function() {
			this._map && this._map.removeLayer(this)
		},
		_Events: function(n) {
			n = n || "on";
			L.Bubble.THIS[n]({
				exitBubble: this._onexit,
				forwardBubble: this._onforward,
				itemBubble: this._onitem,
				backwardBubble: this._onbackward,
				fieldnameBubble: this._onfieldnamedirty,
				fieldaliasBubble: this._onfieldaliasdirty,
				dirtyBubble: this._ondirty,
				deleteFieldBubble: this._ondeleteField,
				submitBubble: this._onsubmit,
				addFieldBubble: this._onaddField
			}, this)
		},
		onAdd: function(n) {
			if(this._New) {
				L.Html.prototype.onAdd.call(this, n);
				n.on(this._getEvents(), this);
				var t = "openBubble";
				this.fire(t);
				n.fire(t, {
					feature: this._bindsource,
					Bubble: this
				});
				this._bindsource && this._bindsource.fire(t, {
					Bubble: this
				});
				this._Events();
				this._adjustPan()
			}
		},
		onRemove: function() {
			var n, t;
			this._New && (n = "closeBubble", this.fire(n), this._map.fire(n, {
				feature: this._bindsource
			}), this._bindsource && this._bindsource.fire(n), this._Events("off"), this._divEvents && this._switchevents("off"), this._map.off(this._getEvents(), this), L.Html.prototype.onRemove.call(this), t = _(this.id + "_div"), t && t.parentNode.removeChild(t), L.Bubble.THIS = null)
		},
		setLatLng: function(n) {
			return this._New && (this.setHtmlLatLng(n), this._adjustPan()), this
		},
		setContent: function(n) {
			var i, t, r;
			return this._New && (this._divEvents && this._switchevents("off"), this._content = n, i = this.set, i.ISarray = L.Util.isArray(n) ? !0 : !1, t = this.options, t.contentwidth = t.contentwidth || (i.ISarray ? 440 : 260), t.contentheight = t.contentheight || (i.ISarray ? 248 : 126), t.width = t.contentwidth + 16 + 16, t.height = t.contentheight + 40 + 85, this.setHtmlOption(t, !0, !1), i.pointer = -1, i.page = 0, i.isdirty = !1, i.items = Math.floor((t.width - 137) / 77) + 1, this._alter(), this.setHtmlContent(this._frame()), r = setInterval(L.bind(function() {
				_(this.id + "_div") && (clearInterval(r), this._showitem(), this._switchevents("on"), this._adjustPan())
			}, this), 5)), this
		},
		getContent: function() {
			return this._content
		},
		_switchevents: function(n) {
			n = n || "on";
			var t = _(this.id + "_div");
			L.DomEvent[n](t, L.DOMtrigger.DOMevents.join(" "), function(n) {
				L.DomEvent.stopPropagation(n);
				n.type === "contextmenu" && L.DomEvent.preventDefault(n)
			}, this);
			this._divEvents = n === "on" ? !0 : !1
		},
		_getEvents: function() {
			return {
				preclick: this._onpreclick,
				moveend: this._onkeepInView
			}
		},
		_onkeepInView: function() {
			this.set.isdirty && this._adjustPan()
		},
		_onpreclick: function(n) {
			this.set.isdirty || this._onexit(n)
		},
		_alter: function() {
			this.set.pages = this.set.ISarray ? Math.floor((this._content.length - 1) / this.set.items) + 1 : 1
		},
		_frame: function() {
			var i = this.options,
				u = i.width,
				f = i.height,
				o = +i.type,
				s = u - 38,
				e = f - 152,
				r = this.id,
				t, n;
			return this.set.MENUid = r + "_MENU", this.set.HTMLid = r + "_HTML", this.set.BACKWARDid = r + "_BACK", this.set.FORWARDid = r + "_NEXT", this.set.FIELDNAMEid = r + "_FIELDNAME", this.set.FIELDALIASid = r + "_FIELDALIAS", this.set.FIELDSUBMITid = r + "_FIELDSUBMIT", this.set.FIELDADDbeforeid = r + "_FIELDADDbefore", this.set.FIELDADDafterid = r + "_FIELDADDafter", this.set.FIELDDELETEid = r + "_FIELDDELETE", this.set.FIELDNEWid = r + "_FIELDNEW", t = this.set, n = [], n.push('<table><tr><td><div id="' + r + '_div"><table><tr><td>'), n.push('<table style="height:40px;"><tr><td style="width:11px;vertical-align:bottom;"><img alt="" src="' + L.iPath + 'popup/lt.png" style="display:block" /><\/td><td id="' + t.MENUid + '" style="width:' + (u - 11 - (i.closeButton ? 22 : 11)) + "px;" + (t.ISarray ? "" : "background-image:url('" + L.iPath + "popup/tbar.png'); background-repeat:repeat-x;") + '"><\/td><td style="width:11px;vertical-align:bottom;"><img alt="" src="' + L.iPath + "popup/rt" + (i.closeButton ? "close" : "") + '.png" ' + (i.closeButton ? 'style="display:block;cursor:pointer;" title="关闭" onclick="L.Bubble.THIS.fire(\'exitBubble\');"' : 'style="display:block;"') + "/><\/td><\/tr><\/table>"), n.push("<\/td><\/tr><tr><td>"), n.push('<table style="width:100%;"><tr>'), n.push('<td style="width:16px;"><table style="width:100%;"><tr><td><img alt="" src="' + L.iPath + 'popup/tl.png" style="display:block"/><\/td><\/tr><tr><td style="width:16px;height:' + (f - 137) + "px; background-image:url('" + L.iPath + 'popup/lbar.png\');background-repeat:repeat-y;"><\/td><\/tr><tr><td><img alt="" src="' + L.iPath + 'popup/bl.png" style="display:block"/><\/td><\/tr><\/table><\/td>'), n.push('<td style="background-color:#ffffff;">'), t.ISarray ? (n.push('<table style="width:100%;height:' + (f - 125) + 'px;">'), n.push('<tr><td style="width:100%;height:22px;"><table style="width:100%;"><tr>'), n.push('<td><img id="' + t.BACKWARDid + '" alt="" src="' + L.iPath + 'popup/backward0.png" style="display:block" title="后退" onclick="L.Bubble.THIS.fire(\'backwardBubble\');"/><\/td>'), n.push('<td style="width: ' + (u - 96) + "px;background-image:url('" + L.iPath + 'popup/titlebar.png\'); background-repeat:repeat-x;overflow:hidden;"><div align="center"><table><tr><td><img alt="" src="' + L.iPath + (o === 0 ? "point" : o === 1 ? "line" : o === 2 ? "polygon" : o === 3 ? "image" : "list") + '.gif" style="display:block" /><\/td><td style="overflow: hidden">&nbsp;' + (i.edit ? "编辑" : "浏览") + "<\/td><\/tr><\/table><\/div><\/td>"), n.push('<td><img id="' + t.FORWARDid + '" alt="" src="' + L.iPath + 'popup/forward0.png" style="display:block" title="前进" onclick="L.Bubble.THIS.fire(\'forwardBubble\');"/><\/td>'), n.push('<\/tr><\/table><\/td><\/tr><tr><td style="vertical-align:top;">'), n.push('<div style="padding:2px;width:' + (u - 38) + "px;height:" + (f - 152) + 'px;border-color:#ababab;border-style:none solid solid solid;border-width:1px;background-color:#d6d6d6;">'), n.push('<table style="width:100%;height:' + (e - (i.edit ? 33 : 0)) + 'px;"border="0">'), n.push('<tr><td style="height:100%;"><table style="width:100%;height:100%;" border="0"><tr><td style="height:22px;"><table style="width:100%;height:100%;" border="0">'), n.push('<tr><td style="border-color:#848484 #ffffff #ffffff #848484;border-style:solid;border-width:1px;">'), n.push(i.edit ? '<input id="' + t.FIELDNAMEid + '" type="text" style="width:99%;height:20px;border-style:none;overflow:hidden;background-color:#d6d6d6;" onchange="L.Bubble.THIS.fire(\'fieldnameBubble\');" onclick="this.focus();"/>' : '<div id="' + t.FIELDNAMEid + '" style="width:100%;height:20px;overflow-x:hidden;overflow-y:auto;-webkit-overflow-scrolling:touch;"><\/div>'), n.push('<\/td><td style="width:2px"><\/td><td style="border-color:#848484 #ffffff #ffffff #848484;border-style:solid;border-width:1px;">'), n.push(i.edit ? '<input id="' + t.FIELDALIASid + '" type="text" style="width:99%;height:20px;border-style:none;overflow:hidden;background-color:#d6d6d6;" onchange="L.Bubble.THIS.fire(\'fieldaliasBubble\');" onclick="this.focus();"/>' : '<div id="' + t.FIELDALIASid + '" style="width:100%;height:20px;overflow-x:hidden;overflow-y:auto;-webkit-overflow-scrolling:touch;"><\/div>'), n.push('<\/td><\/tr><\/table><\/td><\/tr><tr><td style="height:2px"><\/td><\/tr><tr><td style="border-color:#848484 #ffffff #ffffff #848484;border-style:solid;border-width:1px;">'), n.push(i.edit ? '<textarea id="' + t.HTMLid + '" rows="1" cols="1" style="resize:none;font-size:12px;border-style:none;overflow:auto;-webkit-overflow-scrolling:touch;width: ' + (s - 32) + "px;height:" + (e - 63) + 'px; background-color:#d6d6d6;vertical-align:top;text-align:left;" onchange="L.Bubble.THIS.fire(\'dirtyBubble\');" onclick="this.focus();"><\/textarea>' : '<div id="' + t.HTMLid + '" style="font-size:12px;overflow:auto;-webkit-overflow-scrolling:touch;width:' + (s - 2) + "px;height:" + (e - 28) + 'px;background-color:#d6d6d6;vertical-align:top;text-align:left;"><\/div>'), n.push("<\/td><\/tr><\/table><\/td>"), i.edit && (n.push('<td style="width:4px;"><\/td><td style="width:30px;"><table style="width:100%;height: ' + (e - 33) + 'px;" border="0"><tr>'), n.push('<td id="' + t.FIELDSUBMITid + '" class="buttonstyle" style="height:' + (e - 39) / 2 + 'px;" align="center" valign="middle" title="提交" onmouseover="this.focus();this.className=\'buttonoverstyle\';" onmouseout="this.className=\'buttonstyle\';" onclick="this.className=\'buttondownstyle\';L.Bubble.THIS.fire(\'submitBubble\');">'), n.push('<img alt="" src="' + L.iPath + 'available.gif" style="display:block" /><\/td><\/tr><tr><td><\/td><\/tr><tr>'), n.push('<td id="' + t.FIELDDELETEid + '" class="buttonstyle" style="height:' + (e - 39 - (e - 39) / 2) + 'px;" align="center" valign="middle" title="删除" onmouseover="this.focus();this.className=\'buttonoverstyle\';" onmouseout="this.className=\'buttonstyle\';" onclick="this.className=\'buttondownstyle\';L.Bubble.THIS.fire(\'deleteFieldBubble\');">'), n.push('<img alt="" src="' + L.iPath + 'delete.gif" style="display:block" /><\/td><\/tr><\/table><\/td><\/tr><\/table><div style="margin:4px 0px 4px 0px;height:1px;background-color:#848484;width:100%;"><\/div>'), n.push('<table style="width: ' + s + 'px; height:22px;"><tr>'), n.push('<td id="' + t.FIELDADDbeforeid + '" class="buttonstyle" style="width:26px;" align="center" valign="middle" title="插入" onmouseover="this.focus();this.className=\'buttonoverstyle\';" onmouseout="this.className=\'buttonstyle\';" onclick="this.className=\'buttondownstyle\';L.Bubble.THIS.fire(\'addFieldBubble\',{flag:0});">'), n.push('<img alt="" src="' + L.iPath + 'popup/addbefore.png" style="display:block"/><\/td><td style="width:4px;"><\/td><td style="border-color:#848484 #ffffff #ffffff #848484;border-style:solid;border-width:1px;overflow:hidden;" title="新建">'), n.push('<input id="' + t.FIELDNEWid + '" type="text" style="width:99%;border-style:none;overflow:hidden;background-color:#d6d6d6;" onclick="this.focus();"/><\/td><td style="width:4px;"><\/td>'), n.push('<td id="' + t.FIELDADDafterid + '" class="buttonstyle" style="width:26px;" align="center" valign="middle" title="追加" onmouseover="this.focus();this.className=\'buttonoverstyle\';" onmouseout="this.className=\'buttonstyle\';" onclick="this.className=\'buttondownstyle\';L.Bubble.THIS.fire(\'addFieldBubble\',{flag:1});">'), n.push('<img alt="" src="' + L.iPath + 'popup/addafter.png" style="display:block" /><\/td>')), n.push("<\/tr><\/table><\/div><\/td><\/tr><\/table>")) : (n.push('<table style="width:100%;height: ' + (f - 125) + 'px;">'), n.push("<tr>"), i.edit ? (n.push('<td><textarea id="' + t.HTMLid + '" rows="1" cols="1" style="width:' + (u - 71) + "px;height:" + (f - 135) + 'px;vertical-align:top;text-align:left;padding:2px;resize:none;font-size:12px;border-color:#ababab;border-style:solid solid solid solid;border-width:1px;background-color:#d6d6d6;overflow:auto;-webkit-overflow-scrolling:touch;" onchange="L.Bubble.THIS.fire(\'dirtyBubble\');" onclick="this.focus();"><\/textarea>'), n.push('<\/td><td id="' + t.FIELDSUBMITid + '" class="buttonstyle" style="width:30px;" align="center" valign="middle" title="提交" onmouseover="this.focus();this.className=\'buttonoverstyle\';" onmouseout="this.className=\'buttonstyle\';" onclick="this.className=\'buttondownstyle\';L.Bubble.THIS.fire(\'submitBubble\');">'), n.push('<img alt="" src="' + L.iPath + 'available.gif" style="display:block" /><\/td>')) : n.push('<td><div id="' + t.HTMLid + '" style="padding:2px;width:' + (u - 39) + "px; height:" + (f - 131) + 'px;border-color:#ababab;border-style:solid solid solid solid;border-width:1px;background-color:#d6d6d6;overflow:auto;-webkit-overflow-scrolling:touch;"><\/div><\/td>'), n.push("<\/tr><\/table>")), n.push("<\/td>"), n.push('<td style="width:16px;"><table style="width:100%;"><tr><td><img alt="" src="' + L.iPath + 'popup/tr.png" style="display:block"/><\/td><\/tr><tr><td style="width:16px;height:' + (f - 137) + "px;background-image:url('" + L.iPath + 'popup/rbar.png\');background-repeat:repeat-y;"><\/td><\/tr><tr><td><img alt="" src="' + L.iPath + 'popup/br.png" style="display:block"/><\/td><\/tr><\/table><\/td><\/tr><\/table>'), n.push("<\/td><\/tr><tr><td>"), n.push('<table style="height:16px;"><tr><td style="width:22px;vertical-align:top;"><img alt="" src="' + L.iPath + 'popup/lb.png" style="display:block" /><\/td><td style="width:' + parseInt((u - 135) / 2) + "px;vertical-align:top;background-image:url('" + L.iPath + "popup/bbar.png');background-repeat:repeat-x;\"><\/td><td style=\"width:91px;background-image:url('" + L.iPath + 'popup/ptop.png\');background-repeat:no-repeat;"><\/td><td style="width:' + (u - 44 - parseInt((u - 135) / 2) - 91) + "px;vertical-align:top;background-image:url('" + L.iPath + 'popup/bbar.png\');background-repeat:repeat-x;"><\/td><td style="width:22px;vertical-align:top;"><img alt="" src="' + L.iPath + 'popup/rb.png" style="display:block" /><\/td><\/tr><\/table>'), n.push("<\/td><\/tr><\/table><\/div><\/td><\/tr><tr><td>"), n.push('<table style="height:69px;"><tr><td style="width:' + parseInt((u - 91) / 2) + 'px;"><\/td><td style="width:91px;background-image:url(\'' + L.iPath + "popup/p.png');background-repeat:no-repeat;\"><\/td><td ><\/td><\/tr><\/table>"), n.push("<\/td><\/tr><\/table>"), L.Util.toDOM(n.join(""))
		},
		_showitem: function(n) {
			var t, u, i, h, o, c, s;
			n = n || 0;
			t = this.set;
			t.pointer = n;
			t.page = Math.floor(n / t.items);
			var f = n % t.items,
				e = t.page * t.items,
				r = this._content;
			if(t.ISarray) {
				for(u = [], _(t.BACKWARDid).src = L.iPath + "popup/backward" + (t.page > 0 ? "" : "0") + ".png", _(t.BACKWARDid).style.cursor = t.page > 0 ? "pointer" : "", _(t.FORWARDid).src = L.iPath + "popup/forward" + (t.page < t.pages - 1 ? "" : "0") + ".png", _(t.FORWARDid).style.cursor = t.page < t.pages - 1 ? "pointer" : "", u.push('<table style="height:40px;width:100%;"><tr>'), i = 0; i < t.items; i++)
					if(e + i < r.length) h = !0, o = r[e + i].name, L.Util.stringcharlength(o) > 10 && (o = L.Util.stringcharleft(o, 8) + "..", h = !1), i === 0 && u.push("<td style=\"width:26px;background-image:url('" + L.iPath + "popup/tab0" + (f === i ? "s" : "") + ".png');background-repeat:no-repeat;\"><\/td>"), u.push("<td " + (h ? "" : 'title="' + r[e + i].name + '"') + ' style="' + (f === i ? "" : "cursor:pointer;") + "width:52px;background-image:url('" + L.iPath + "popup/tbar" + (f === i ? "s" : "n") + ".png');background-repeat:repeat-x;\"" + (f === i ? "" : "onclick=\"L.Bubble.THIS.fire('itemBubble',{code:" + (e + i) + '});"') + '><table style="width:100%;"><tr><td style="text-align:center;vertical-align:middle;white-space:nowrap;">' + o + '<\/td><\/tr><tr><td style="height:15px">&nbsp;<\/td><\/tr><\/table><\/td>'), i === t.items - 1 || e + i === r.length - 1 ? u.push("<td style=\"width:26px;background-image:url('" + L.iPath + "popup/tab1" + (f === i ? "s" : "") + ".png');background-repeat:no-repeat;\"><\/td>") : u.push("<td style=\"width:25px;background-image:url('" + L.iPath + "popup/tab" + (f === i ? "2s" : i + 1 === f ? "3s" : "2") + ".png');background-repeat:no-repeat;\"><\/td>");
					else break;
				u.push("<td style=\"background-image:url('" + L.iPath + "popup/tbar.png');background-repeat:repeat-x\">&nbsp;<\/td>");
				u.push("<\/tr><\/table>");
				c = _(t.MENUid);
				c = L.Util.innerhtml2dom(_(t.MENUid), u.join(""))
			}
			this.options.edit ? (t.ISarray ? (_(t.FIELDNAMEid).value = r[n].name, _(t.FIELDALIASid).value = r[n].alias, s = L.Util.isArray(r[n].content) ? OBJECT.stringify(r[n].content) : r[n].content) : s = L.Util.isArray(r) ? OBJECT.stringify(r) : r, _(t.HTMLid).value = s === '""' ? "" : s) : t.ISarray ? (_(t.FIELDNAMEid).innerHTML = r[n].name, _(t.FIELDALIASid).innerHTML = r[n].alias, _(t.HTMLid).innerHTML = L.Util.sheet(r[n].content)) : _(t.HTMLid).innerHTML = L.Util.sheet(r)
		},
		_onfieldaliasdirty: function() {
			var n = this.set;
			this._content[n.pointer].alias = L.Util.trim(_(n.FIELDALIASid).value);
			n.isdirty = !0
		},
		_onfieldnamedirty: function() {
			var n = this.set,
				t = n.pointer,
				i = L.Util.trim(_(n.FIELDNAMEid).value);
			this._fieldname(i) ? (n.isdirty = !0, this._content[t].name = i, this._showitem(t)) : (_(n.FIELDNAMEid).value = this._content[t].name, n.isdirty = !1)
		},
		_ondirty: function() {
			var n = this.set,
				t = L.Util.trim(_(n.HTMLid).value);
			n.ISarray ? this._content[n.pointer].content = t : this._content = t;
			n.isdirty = !0
		},
		_onitem: function(n) {
			this._showitem(n.code)
		},
		_onexit: function(t) {
			t && L.DomEvent.stop(t);
			var i = !0;
			return this.set.isdirty && (n.confirm("被更改的内容尚未提交，确认退出吗？") || (i = !1)), i && this._map.removeLayer(this), i
		},
		_fieldname: function(n) {
			var t = null,
				i;
			if(n.length === 0) t = "名称不能为空！";
			else if(n.length > 255) t = "名称太长！";
			else
				for(i = 0; i < this._content.length; i++)
					if(n === this._content[i].name) {
						t = "名称已存在！";
						break
					} return t != null ? (alert(t), !1) : !0
		},
		_onaddField: function(n) {
			var t = this.set,
				r = L.Util.trim(_(t.FIELDNEWid).value),
				i;
			if(this._fieldname(r)) {
				i = {
					name: r,
					alias: "",
					content: ""
				};
				switch(n.flag) {
					case 0:
						this._content.splice(t.pointer, 0, i);
						break;
					default:
						this._content.splice(++t.pointer, 0, i)
				}
				t.isdirty = !0;
				this._alter();
				this._showitem(t.pointer)
			}
		},
		_ondeleteField: function() {
			if(this._content.length <= 1) alert("至少保留一个标签！");
			else if(n.confirm("确认删除吗？")) {
				var t = this.set;
				t.isdirty = !0;
				this._content.splice(t.pointer, 1);
				t.pointer === this._content.length && t.pointer--;
				this._alter();
				this._showitem(t.pointer)
			}
		},
		_onsubmit: function(n) {
			var r, t, i;
			n && L.DomEvent.stop(n);
			r = this.set;
			r.isdirty && (t = this._content, i = "featurePropertyChanged", this.fire(i, {
				content: t
			}), this._bindsource && (this._bindsource._bindBubbleContent = t, this._bindsource.fire(i, {
				content: t
			})), this._map.fire(i, {
				feature: this._bindsource,
				content: t
			}), r.isdirty = !1)
		},
		_onforward: function() {
			var n = this.set;
			n.page < n.pages - 1 && this._showitem((n.page + 1) * n.items)
		},
		_onbackward: function() {
			var n = this.set;
			n.page > 0 && this._showitem(n.page * n.items - 1)
		},
		_adjustPan: function() {
			var u = this._map,
				c = this._latlng,
				n;
			if(u && c && this._html) {
				if(n = this.options, !n.autoPan) return;
				var s = n.height,
					h = n.width,
					t = u.latLngToContainerPoint(c)._subtract({
						x: h / 2 - n.dx,
						y: s
					}),
					l = L.point(n.autoPanPadding),
					f = L.point(n.autoPanPaddingTopLeft || l),
					e = L.point(n.autoPanPaddingBottomRight || l),
					o = u.getSize(),
					i = 0,
					r = 0;
				t.x + h + e.x > o.x && (i = t.x + h + e.x - o.x);
				t.x - i - f.x < 0 && (i = t.x - f.x);
				t.y + s + e.y > o.y && (r = t.y + s + e.y - o.y);
				t.y - r - f.y < 0 && (r = t.y - f.y);
				(i || r) && u.panBy([i, r])
			}
		}
	});
	L.bubble = function(n, t) {
		return new L.Bubble(n, t)
	};
	L.Handler._PointDrag = L.Handler.MarkerDrag.extend({
		statics: {
			THIS: null
		},
		initialize: function(n, t) {
			if(L.Handler.MarkerDrag.prototype.initialize.call(this, n), this._map = n._map, this._map) {
				var i = L.Handler._PointDrag.THIS;
				i && i.disable();
				L.Handler._PointDrag.THIS = this;
				t && L.setOptions(this, t)
			}
		},
		addHooks: function() {
			L.Handler.MarkerDrag.prototype.addHooks.call(this);
			this._marker.options.select || (this._marker._icon.style.outline = "#ff0000 dashed 1px")
		},
		removeHooks: function() {
			this._marker.options.select || (this._marker._icon.style.outline = "");
			L.Handler.MarkerDrag.prototype.removeHooks.call(this)
		}
	});
	L.Handler._ImageDrag = L.Handler.extend({
		statics: {
			THIS: null
		},
		options: {
			opacity: .8,
			icon: new L.DivIcon({
				className: "leaflet-div-icon"
			})
		},
		initialize: function(n, t) {
			this._marker = n;
			this._map = n._map;
			var i = L.Handler._ImageDrag.THIS;
			i && i.disable();
			L.Handler._ImageDrag.THIS = this;
			t && L.setOptions(this, t);
			this._markers = []
		},
		addHooks: function() {
			this._markerGroup || this._initMarkers();
			this._map.addLayer(this._markerGroup);
			this._moved = !1;
			this._marker.options.select || (this._marker._icon.style.outline = "#ff0000 dashed 1px")
		},
		removeHooks: function() {
			var n = this._markers;
			for(var t in n) n.hasOwnProperty(t) && n[t].off("dragstart", this._onNodeDragStart, this).off("drag", this._onNodeDrag, this).off("dragend", this._onNodeDragEnd, this);
			this._map.removeLayer(this._markerGroup);
			delete this._markerGroup;
			delete n;
			this._moved = !1;
			this._marker.options.select || (this._marker._icon.style.outline = "")
		},
		_initMarkers: function() {
			this._markerGroup || (this._markerGroup = new L.LayerGroup);
			this._markers = [];
			for(var n = 4; n >= 0; n--) this._markers.push(this._createMarker(n))
		},
		_createMarker: function(n) {
			var i = this._getnodelatlng(n),
				t = L.marker(i, {
					order: n,
					draggable: !0,
					pane: "tooltipPane",
					icon: this.options.icon
				});
			t.on("dragstart", this._onNodeDragStart, this).on("drag", this._onNodeDrag, this).on("dragend", this._onNodeDragEnd, this);
			return this._markerGroup.addLayer(t), t
		},
		moved: function() {
			return this._moved
		},
		_getnodelatlng: function(n) {
			var i = this._marker._bounds,
				t;
			switch(parseInt(n)) {
				case 1:
					t = i.getSouthWest();
					break;
				case 2:
					t = i.getNorthWest();
					break;
				case 3:
					t = i.getNorthEast();
					break;
				case 4:
					t = i.getSouthEast();
					break;
				default:
					t = i.getCenter()
			}
			return t.clone()
		},
		_onNodeDragStart: function() {
			this._marker.fire("dragstart")
		},
		_onNodeDrag: function(n) {
			var o = n.target.options.order,
				u = n.target._latlng,
				s = this._getnodelatlng(o),
				i = u.lat - s.lat,
				r = u.lng - s.lng,
				f, e, t;
			switch(o) {
				case 0:
					this._markers[0].setLatLng(u);
					t = this._getnodelatlng(1);
					this._markers[1].setLatLng(f = L.latLng(t.lat + i, t.lng + r));
					t = this._getnodelatlng(2);
					this._markers[2].setLatLng(L.latLng(t.lat + i, t.lng + r));
					t = this._getnodelatlng(3);
					this._markers[3].setLatLng(e = L.latLng(t.lat + i, t.lng + r));
					t = this._getnodelatlng(4);
					this._markers[4].setLatLng(L.latLng(t.lat + i, t.lng + r));
					break;
				case 1:
					this._markers[1].setLatLng(f = u);
					t = this._getnodelatlng(2);
					this._markers[2].setLatLng(L.latLng(t.lat, t.lng + r));
					this._markers[3].setLatLng(e = this._getnodelatlng(3));
					t = this._getnodelatlng(4);
					this._markers[4].setLatLng(L.latLng(t.lat + i, t.lng));
					break;
				case 2:
					t = this._getnodelatlng(1);
					this._markers[1].setLatLng(f = L.latLng(t.lat, t.lng + r));
					this._markers[2].setLatLng(u);
					t = this._getnodelatlng(3);
					this._markers[3].setLatLng(e = L.latLng(t.lat + i, t.lng));
					this._markers[4].setLatLng(this._getnodelatlng(4));
					break;
				case 3:
					this._markers[1].setLatLng(f = this._getnodelatlng(1));
					t = this._getnodelatlng(2);
					this._markers[2].setLatLng(L.latLng(t.lat + i, t.lng));
					this._markers[3].setLatLng(e = u);
					t = this._getnodelatlng(4);
					this._markers[4].setLatLng(L.latLng(t.lat, t.lng + r));
					break;
				case 4:
					t = this._getnodelatlng(1);
					this._markers[1].setLatLng(f = L.latLng(t.lat + i, t.lng));
					this._markers[2].setLatLng(this._getnodelatlng(2));
					t = this._getnodelatlng(3);
					this._markers[3].setLatLng(e = L.latLng(t.lat, t.lng + r));
					this._markers[4].setLatLng(u)
			}
			o !== 0 && (t = this._getnodelatlng(0), this._markers[0].setLatLng(L.latLng(t.lat + i / 2, t.lng + r / 2)));
			this._marker.setBounds(L.latLngBounds(f, e));
			this._marker.fire("drag")
		},
		_onNodeDragEnd: function() {
			this._moved = !0;
			this._marker.fire("dragend")
		}
	});
	L.Handler._PathDrag = L.Handler.extend({
		statics: {
			THIS: null
		},
		options: {
			icon: new L.DivIcon({
				className: "leaflet-div-icon"
			}),
			nodeopacity: 1,
			middleopacity: .5,
			dashArray: "10,8"
		},
		initialize: function(n, t) {
			if(this._marker = n, this._map = n._map, this._map) {
				var i = L.Handler._PathDrag.THIS;
				i && i.disable();
				L.Handler._PathDrag.THIS = this;
				this._type = n.type;
				this._latlngs = this._marker.getLatLngs();
				L.setOptions(this, t)
			}
		},
		addHooks: function() {
			this._markerGroup || this._initMarkers();
			this._map.addLayer(this._markerGroup);
			this._moved = !1;
			this._marker.setStyle({
				color: this._marker.options.select === !0 ? "#ffff00" : this._marker.color,
				dashArray: this.options.dashArray
			})
		},
		removeHooks: function() {
			var n = this._markers;
			for(var t in n) n.hasOwnProperty(t) && n[t].off("drag", this._onMarkerDrag, this).off("dragend", this._fireEvents, this).off("click", this._onMarkerClick, this);
			this._map.removeLayer(this._markerGroup);
			delete this._markerGroup;
			delete n;
			this._moved = !1;
			this._marker.setStyle({
				color: this._marker.options.select === !0 ? "#ffff00" : this._marker.color,
				dashArray: null
			})
		},
		_initMarkers: function() {
			var t, n, r, i, u, f, e;
			for(this._markerGroup || (this._markerGroup = new L.LayerGroup), this._markers = [], t = this._latlngs, this._type === 2 && (t = this._latlngs = L.Util.Polygons(t)[0]), n = 0, i = t.length; n < i; n++) {
				u = this._createMarker(t[n], n);
				u.on("click", this._onMarkerClick, this);
				this._markers.push(u)
			}
			for(n = 0, r = i - 1; n < i; r = n++)(n !== 0 || this._type !== 1) && (f = this._markers[r], e = this._markers[n], this._createMiddleMarker(f, e), this._updatePrevNext(f, e))
		},
		_createMarker: function(n, t) {
			var i = L.marker(n, {
				draggable: !0,
				icon: this.options.icon
			});
			i._origLatLng = n;
			i._index = t;
			i.on("drag", this._onMarkerDrag, this);
			i.on("dragend", this._fireEvents, this);
			return this._markerGroup.addLayer(i), i
		},
		_removeMarker: function(n) {
			var t = n._index;
			this._markerGroup.removeLayer(n);
			this._markers.splice(t, 1);
			this._spliceLatLngs(t, 1);
			this._updateIndexes(t, -1);
			n.off("drag", this._onMarkerDrag, this).off("dragend", this._fireEvents, this).off("click", this._onMarkerClick, this)
		},
		moved: function() {
			return this._moved
		},
		_fireEvents: function() {
			this._moved = !0;
			this._marker.fire("dragend")
		},
		_onMarkerDrag: function(n) {
			var t = n.target;
			L.extend(t._origLatLng, t._latlng);
			t._middleLeft && t._middleLeft.setLatLng(this._getMiddleLatLng(t._prev, t));
			t._middleRight && t._middleRight.setLatLng(this._getMiddleLatLng(t, t._next));
			this._marker.redraw();
			this._marker.fire("drag")
		},
		_onMarkerClick: function(n) {
			this._marker.fire("dragstart");
			var i = this._marker instanceof L.Polygon ? 4 : 3,
				t = n.target;
			this._latlngs.length < i || (this._removeMarker(t), this._updatePrevNext(t._prev, t._next), t._middleLeft && this._markerGroup.removeLayer(t._middleLeft), t._middleRight && this._markerGroup.removeLayer(t._middleRight), t._prev && t._next ? this._createMiddleMarker(t._prev, t._next) : t._prev ? t._next || (t._prev._middleRight = null) : t._next._middleLeft = null, this._fireEvents())
		},
		_spliceLatLngs: function() {
			var n = [].splice.apply(this._latlngs, arguments);
			return this._marker._convertLatLngs(this._latlngs, !0), this._marker.redraw(), n
		},
		_updateIndexes: function(n, t) {
			this._markerGroup.eachLayer(function(i) {
				i._index > n && (i._index += t)
			})
		},
		_createMiddleMarker: function(n, t) {
			var r = this._getMiddleLatLng(n, t),
				i = this._createMarker(r),
				e, u, f;
			i.setOpacity(this.options.middleopacity);
			n._middleRight = t._middleLeft = i;
			u = function() {
				var u = t._index;
				i._index = u;
				i.off("click", e, this).on("click", this._onMarkerClick, this);
				r.lat = i.getLatLng().lat;
				r.lng = i.getLatLng().lng;
				this._spliceLatLngs(u, 0, r);
				this._markers.splice(u, 0, i);
				i.setOpacity(this.options.nodeopacity);
				this._updateIndexes(u, 1);
				t._index++;
				this._updatePrevNext(n, i);
				this._updatePrevNext(i, t);
				this._marker.fire("editStart")
			};
			f = function() {
				i.off("dragstart", u, this).off("dragend", f, this);
				this._createMiddleMarker(n, i);
				this._createMiddleMarker(i, t)
			};
			e = function() {
				u.call(this);
				f.call(this);
				this._fireEvents()
			};
			i.on("click", e, this).on("dragstart", u, this).on("dragend", f, this);
			this._markerGroup.addLayer(i)
		},
		_updatePrevNext: function(n, t) {
			n && (n._next = t);
			t && (t._prev = n)
		},
		_getMiddleLatLng: function(n, t) {
			var i = this._map,
				r = i.project(n.getLatLng()),
				u = i.project(t.getLatLng());
			return i.unproject(r._add(u)._divideBy(2))
		}
	});
	L.__bind = {
		_bindLatLng: null,
		_bindHtmlObject: null,
		_bindHtmlContent: null,
		type: null,
		_bindHtmlTip: null,
		_bindBubbleSelf: !1,
		_bindBubbleOptions: null,
		_bindBubbleContent: null,
		_getBindType: function(n) {
			var t = n ? n : this,
				i = t instanceof L.GroundOverlay ? 3 : t instanceof L.Polygon ? 2 : t instanceof L.Polyline ? 1 : t instanceof L.Symbol ? 0 : t instanceof L.CircleMarker ? -1 : t instanceof L.Circle ? -2 : t instanceof L.Rectangle ? -3 : null;
			return n ? i : this.type = i
		},
		_getEventLatLng: function(n) {
			var t = null,
				i = this._map;
			switch(this.type) {
				case 3:
				case 0:
					n = n.touches && n.touches.length === 1 ? n.touches[0] : n;
					t = isNaN(n.clientX) || isNaN(n.clientY) ? n.latlng || n.target._latlng : i.containerPointToLatLng(i.mouseEventToContainerPoint(n));
					break;
				case 2:
				case 1:
				case -1:
				case -2:
				case -3:
					t = n.latlng
			}
			return t
		},
		_getBindLatLng: function() {
			var n = null;
			switch(this.type) {
				case 3:
				case 2:
				case 1:
					n = this.getCenter();
					break;
				case 0:
				case -1:
				case -2:
				case -3:
					n = this.getLatLng()
			}
			return n
		},
		selectStatus: function(n) {
			var t = this.options.select = typeof n == "boolean" ? n : !this.options.select;
			switch(this._getBindType(this)) {
				case 3:
					this.setStyle({
						select: t
					});
					break;
				case 2:
				case 1:
				case -1:
				case -2:
				case -3:
					this.setStyle({
						color: t === !0 ? "#ffff00" : this.color
					});
					break;
				case 0:
					this.options.icon.setStyle && this.options.icon.setStyle({
						select: t
					})
			}
		},
		featureGeometry: function(n) {
			var i = !0,
				t = NULL(n);
			switch(this.type) {
				case 3:
					if(t) return this.getBounds();
					this.setBounds(n);
					break;
				case 0:
					if(t) return this.getLatLng();
					this.setLatLng(n);
					break;
				case 2:
				case 1:
				case -1:
				case -2:
				case -3:
					if(t) return this.getLatLngs();
					this.setLatLngs(n);
					break;
				default:
					i = !1
			}
			return !t && i && (this._bindBubbleSelf && (this._Bubble && this._Bubble.remove(), this._bindBubbleSelf = !1, this._Bubble = null), this._map && this._map.fire("featureGeometryChanged", {
				feature: this
			})), this
		},
		featureStyle: function(n) {
			var t = null,
				u = !0,
				r = NULL(n);
			switch(this._getBindType(this)) {
				case 3:
					r ? t = {
						url: this._url,
						opacity: this.options.opacity
					} : (NULL(n.url) || (this._icon.src = this._url = this.options.url = this.options.icon.options.iconUrl = n.url), NULL(n.opacity) || this.setOpacity(n.setOpacity));
					break;
				case 0:
					r ? t = L.extend({
						opacity: this.options.opacity
					}, this.options.icon.options) : (NULL(n.icon) && (NULL(n.markerno) || NULL(n.markertype)) || this.setIcon(this._map.placeMark(n)), NULL(n.opacity) || this.setOpacity(n.setOpacity));
					break;
				case 2:
				case 1:
				case -1:
				case -2:
				case -3:
					r ? t = L.extend({}, this.options) : NULL(n.color) && NULL(n.weight) && NULL(n.opacity) && NULL(n.fillColor) && NULL(n.fillOpacity) || (NULL(n.color) || (this.color = n.color), this.setStyle(n));
					break;
				default:
					u = !1
			}
			if(u) {
				if(r) return L.extend(t, {
					rank: this.rank,
					toolTip: this.toolTip,
					description: this.description,
					toolTipFrom: this.toolTipFrom
				});
				this._map && (NULL(n.rank) && n.toolTip === i && NULL(n.toolTipFrom) || this._map.bindRelation(this, n), this._map.fire("featureStyleChanged", {
					feature: this
				}))
			}
			return this
		},
		featureProperty: function(n) {
			return NULL(n) ? this._bindBubbleContent : (L.Util.isArray(n) && (this._bindBubbleSelf && (this._Bubble && this._Bubble.remove(), this._bindBubbleSelf = !1, this._Bubble = null), this._bindBubbleContent = n.slice(0), this._map && this._map.fire("featurePropertyChanged", {
				feature: this,
				content: this._bindBubbleContent
			})), this)
		},
		bindFeatureEdit: function() {
			return this._bindFeatureEdit !== !0 && (NULL(this.type) && this._getBindType(), this._listenEdit(), this._bindFeatureEdit = !0), this
		},
		_listenEdit: function(n) {
			n = n || "on";
			this.options.draggable && this[n]("mouseover", this._FeatureEditEvent, this);
			this[n]("remove", this.unbindFeatureEdit, this)
		},
		unbindFeatureEdit: function() {
			return this.dragging && (this.dragging.disable(), this.dragging = null), this._bindFeatureEdit === !0 && (this._listenEdit("off"), this._onBindFeaturePostEvents && this._listenEditEvent("off"), this._bindFeatureEdit = !1), this
		},
		_listenEditEvent: function(n) {
			n = n || "on";
			this[n]("drag", this._onBindFeatureDraging, this)[n]("dragend", this._onBindFeatureEditend, this);
			this._map && this._map[n]("click", this._FeatureEditEvent, this)[n]("contextmenu", this._FeatureEditEvent, this);
			this._onBindFeaturePostEvents = n === "on"
		},
		_FeatureEditEvent: function(n) {
			var t = L.Handler._PathDrag.THIS;
			if(n.type === "mouseover") {
				if(t) {
					if(t.BindFeatureDraging) return;
					switch(this.type) {
						case 3:
						case 0:
							t.disable();
							L.Handler._PathDrag.THIS = null;
							break;
						case 2:
						case 1:
						case -1:
						case -2:
						case -3:
							switch(t._marker.type) {
								case 3:
								case 0:
									t.disable();
									L.Handler._PathDrag.THIS = null;
									break;
								default:
									return
							}
							break;
						default:
							return
					}
				}
				this.dragging && (this.dragging.disable(), this.dragging = null);
				switch(this.type) {
					case 3:
					case 0:
						L.Handler._PathDrag.THIS = this.dragging = this.type === 3 ? new L.Handler._ImageDrag(this) : new L.Handler._PointDrag(this);
						break;
					case 2:
					case 1:
					case -1:
					case -2:
					case -3:
						this.dragging = new L.Handler._PathDrag(this)
				}
				this.dragging && (this.dragging.enable(), this._onBindFeaturePostEvents !== !0 && this._listenEditEvent())
			} else t && (t.disable(), this._onBindFeaturePostEvents && this._listenEditEvent("off"), L.Handler._PathDrag.THIS = null)
		},
		_onBindFeatureDraging: function() {
			this.dragging.BindFeatureDraging = !0
		},
		_onBindFeatureEditend: function() {
			this.dragging.BindFeatureDraging = !1;
			this._map.fire("featureGeometryChanged", {
				feature: this
			})
		},
		bindFeatureDelete: function() {
			return this._bindFeatureDelete !== !0 && (NULL(this.type) && this._getBindType(), this._listenDelete()), this
		},
		_listenDelete: function(n) {
			n = n || "on";
			this[n]("contextmenu", this._FeatureDeleteEvent, this)[n]("remove", this.unbindFeatureDelete, this);
			this._bindFeatureDelete = n === "on"
		},
		unbindFeatureDelete: function() {
			return this._bindFeatureDelete && this._listenDelete("off"), this
		},
		_FeatureDeleteEvent: function(t) {
			var i, r, u, f, e;
			this._map && (this.rank === 0 && n.confirm("确认删除吗？") ? (i = L.Handler._PathDrag.THIS, i && i._marker === this && (i.disable(), L.Handler._PathDrag.THIS = null), this._map.removeFeature(this)) : (this.options.select && (this.selectStatus(!1), r = "featureUnSelect", u = {
				feature: this
			}, this.fire(r, u), this._map.fire(r, u)), this._bindHtmlObject && this._bindHtmlTip && this._removeBindHtml(!0), this._bindBubbleSelf && this.closeBubble(), this.bringToBack()), this._map && (f = "featureRightClick", e = {
				evt: t,
				feature: this
			}, this.fire(f, e), this._map.fire(f, e)))
		},
		bindHtml: function(n, t) {
			if(NULL(this.type) && this._getBindType(), this.type !== null) {
				t = t || {};
				t.followmap = !0;
				t.type = this.type;
				t.age = isNaN(t.age) ? 0 : t.age;
				this._bindHtmlTip = t.age > 0 ? !0 : !1;
				this._bindHtmlTip && (t.pane = "popupPane");
				var i;
				i = n && n.length > 0 ? n : this.toolTip && this.toolTip.length > 0 ? this.toolTip : this.toolTipFrom && this.toolTipFrom.length > 0 ? L.Util.dig(this.description, this.toolTipFrom) : null;
				i && (this._bindHtmlContent = i, this._bindhtmloptions = t, this._bindHtmlTip ? isNaN(this._bindhtmloptions.dy) && (this._bindhtmloptions.dy = 25) : this._bindHtmlObject || (this._bindhtmloptions.align || (this._bindhtmloptions.align = "lm"), this._bindHtmlObject = L.html(this._getBindLatLng(), this._bindHtmlContent, this._bindhtmloptions), this._map.addLayer(this._bindHtmlObject)), this._HtmlEvent !== !0 && (this._listenBindHtml(), this._HtmlEvent = !0));
				this._bindHtml = !0
			}
			return this
		},
		_listenBindHtml: function(n) {
			n = n || "on";
			this._bindHtmlTip && this[n]("mousemove", this._moveBindHtml, this)[n]("mouseover", this.openBindHtml, this)[n]("mouseout", this._removeBindHtml, this);
			this[n]("remove", this.unbindHtml, this)[n]("move", this._moveingBindHtml, this)
		},
		unbindHtml: function() {
			return this._HtmlEvent && (this._listenBindHtml("off"), this._bindHtmlObject && this._map && this._map.removeLayer(this._bindHtmlObject), this._bindHtmlObject = null, this._HtmlEvent = !1), this._bindHtml = !1, this
		},
		openBindHtml: function(n) {
			if(!this._Bubble && this._bindHtmlTip === !0 && this._bindHtmlObject === null) {
				var t = this;
				if(n) this._bindHtmlStay && clearTimeout(this._bindHtmlStay), this._bindHtmlStay = setTimeout(L.bind(function() {
					t._bindHtmlObject = L.html(t._bindLatLng, t._bindHtmlContent, t._bindhtmloptions);
					t._map && t._map.addLayer(t._bindHtmlObject);
					t._bindHtmlStay = null;
					t._bindHtmlObject.once("remove", function() {
						t._bindHtmlObject = null
					}, t)
				}, t), 1e3);
				else {
					t._bindHtmlObject = L.html(t._getBindLatLng(), t._bindHtmlContent, t._bindhtmloptions);
					t._map && t._map.addLayer(t._bindHtmlObject);
					t._bindHtmlObject.once("remove", function() {
						t._bindHtmlObject = null
					}, t)
				}
			}
		},
		_moveingBindHtml: function() {
			this._bindHtmlTip ? this._removeBindHtml() : this._moveBindHtml()
		},
		_moveBindHtml: function(n) {
			this._bindLatLng = this._bindHtmlTip ? this._getEventLatLng(n) : this._getBindLatLng();
			this._bindHtmlObject && this._bindHtmlStay == null && this._bindHtmlObject.setHtmlLatLng(this._bindLatLng)
		},
		_removeBindHtml: function(n) {
			this._bindHtmlStay && (clearTimeout(this._bindHtmlStay), this._bindHtmlStay = null);
			this._bindHtmlObject && (n ? (this._bindHtmlObject.remove.call(this._bindHtmlObject, !0), this._bindHtmlObject = null, this._bindLatLng = null) : this._bindHtmlObject.remove.call(this._bindHtmlObject, function(n) {
				return function() {
					n._bindHtmlObject = null;
					n._bindLatLng = null
				}
			}(this)))
		},
		updatebindHtmlContent: function(n) {
			return this._bindHtmlContent = typeof n == "function" ? n() : n, this
		},
		updatebindHtmlOption: function(n) {
			return L.extend(this._bindhtmloptions, n), this
		},
		bindBubble: function(n, t) {
			return NULL(this.type) && this._getBindType(), this.type !== null && (this._bindBubbleContent = n ? n : this.description ? this.description : null, this._bindBubbleContent && (t = t || {}, t.type = this.type, this._bindBubbleOptions = t, this._BubbleEvent !== !0 && (this._listenbindBubble(), this._BubbleEvent = !0)), this._bindBubble = !0), this
		},
		_listenbindBubble: function(n) {
			n = n || "on";
			this[n]("click", this.toggleBubble, this)[n]("remove", this.unbindBubble, this)[n]("move", this.closeBubble, this)
		},
		unbindBubble: function() {
			return this._BubbleEvent && (this._listenbindBubble("off"), this._Bubble && (this._map.removeLayer(this._Bubble), this._Bubble = null), this._BubbleEvent = !1), this._bindBubble = !1, this
		},
		openBubble: function(n) {
			var t, i, r;
			if(!this._bindBubbleSelf && this._bindBubbleContent !== null && this._bindBubbleOptions !== null) {
				this._bindHtmlObject && this._bindHtmlTip && this._removeBindHtml(!0);
				this._bindBubbleSelf = !0;
				this._bindBubbleOptions.dx = this._bindBubbleOptions.dy = 0;
				this.type === 0 || this.type === -1 ? (t = this.getLatLng(), n && (i = this._map.latLngToContainerPoint(t)._subtract(this._map.latLngToContainerPoint(n.latlng)), this._bindBubbleOptions.dx = -i.x, this._bindBubbleOptions.dy = -i.y)) : t = n ? this._getEventLatLng(n) : this._getBindLatLng();
				this._Bubble = L.bubble(this._bindBubbleOptions, this).addTo(this._map).setContent(this._bindBubbleContent).setLatLng(t);
				this._map.fire("bubbleOpened", {
					feature: this
				});
				r = this;
				this._Bubble.once("closeBubble", function() {
					r._bindBubbleSelf = !1;
					r._Bubble = null
				}, this)
			}
			return this
		},
		closeBubble: function() {
			return this._Bubble && this._Bubble.set && !this._Bubble.set.isdirty && (this._Bubble.remove(), this._bindBubbleSelf = !1, this._Bubble = null), this
		},
		toggleBubble: function(n) {
			if(this.bringToFront(), this._bindBubbleSelf ? this.closeBubble() : this.openBubble(n), !this.options.select) {
				this.selectStatus(!0);
				var t = "featureSelect",
					i = {
						feature: this
					};
				this.fire(t, i);
				this._map.fire(t, i)
			}
		},
		updateBubbleOption: function(n) {
			return L.extend(this._bindBubbleOptions, n), this
		},
		updateBubbleContent: function(n) {
			return this._bindBubbleContent = typeof n == "function" ? n() : n, this
		}
	};
	L.GroundOverlay.include(L.__bind);
	L.Symbol.include(L.__bind);
	L.Path.include(L.__bind);
	L.Map.include({
		MapProvider: n.MapProvider || parent.MapProvider || parent.parent.MapProvider,
		baseMap: function(n) {
			var b = n === !0,
				u, s = this.MapProvider,
				t, r, p, e, y, w, o, h, i, f, c, l, a, v;
			if(NULL(s)) return alert("未发现底图对象！"), null;
			if(typeof n != "number") {
				for(u = 0; u < s.length; u++)
					if(s[u].selected) {
						n = u;
						break
					}
				typeof n != "number" && (n = 0)
			}
			for(u = 0; u < s.length; u++) s[u].selected = u === n;
			t = s[n];
			r = {};
			r.minZoom = parseInt(isNaN(t.minzoom) ? 0 : t.minzoom);
			r.maxZoom = parseInt(isNaN(t.maxzoom) ? 18 : t.maxzoom);
			typeof t.copyright == "string" && (r.attribution = L.Util.trim(t.copyright));
			r.opacity = !isNaN(t.opacity) && parseFloat(t.opacity) >= 0 && parseFloat(t.opacity) <= 1 ? parseFloat(t.opacity) : 1;
			t.detectRetina && (r.detectRetina = t.detectRetina);
			typeof t.errorTileUrl == "string" && (r.errorTileUrl = t.errorTileUrl);
			typeof t.tileSize == "number" && (r.tileSize = t.tileSize);
			typeof t.tms == "boolean" && (r.tms = t.tms);
			typeof t.subdomains == "string" && (r.subdomains = L.Util.trim(t.subdomains));
			p = r.subdomains;
			!isNaN(t.west) && t.west >= -180 && !isNaN(t.east) && t.east <= 180 && !isNaN(t.north) && t.north <= 90 && !isNaN(t.south) && t.south >= -90 && (r.bounds = L.latLngBounds([t.south, t.west], [t.north, t.east]));
			e = null;
			y = this._layers;
			for(w in y) y.hasOwnProperty(w) && (o = y[w], o._isBaseMap === !0 && (b ? o._baseMapName === null ? this.removeLayer(o) : e = o : (this.fire("baseMapDeleted", {
				layer: o
			}), this.removeLayer(o))));
			if(h = t.overlayers, t.showoverlayers === !0 && L.Util.isArray(h))
				for(u = h.length - 1; u >= 0; u--) i = h[u], L.Util.trim(i.overlayer).length > 0 && (f = L.extend({}, r), c = i.opacity, !isNaN(c) && parseFloat(c) >= 0 && parseFloat(c) <= 1 && (f.opacity = parseFloat(c)), l = i.minzoom, !isNaN(l) && parseInt(l) >= 0 && parseInt(l) <= 99 && (f.minZoom = parseInt(l)), a = i.maxZoom, !isNaN(a) && parseInt(a) > i.minzoom && parseInt(a) <= 99 && (f.maxZoom = parseInt(a)), i.detectRetina && (f.detectRetina = i.detectRetina), typeof i.errorTileUrl == "string" && (f.errorTileUrl = i.errorTileUrl), typeof i.tileSize == "number" && (f.tileSize = i.tileSize), typeof i.tms == "boolean" && (f.tms = i.tms), typeof i.subdomains == "string" && (f.subdomains = L.Util.trim(i.subdomains)), !isNaN(i.west) && i.west >= -180 && !isNaN(i.east) && i.east <= 180 && !isNaN(i.north) && i.north <= 90 && !isNaN(i.south) && i.south >= -90 && (f.bounds = L.latLngBounds([i.south, i.west], [i.north, i.east])), v = L.tileLayer(i.overlayer, f), v._isBaseMap = !0, v._baseMapName = null, this.addLayer(v), v.bringToBack());
			return e !== null && b || (this.options.maxZoom = r.maxZoom, this.options.minZoom = r.minZoom, this.getZoom() > r.maxZoom && this.setZoom(r.maxZoom), r.subdomains = typeof p == "string" ? p : L.TileLayer.prototype.options.subdomains, e = L.tileLayer(t.baselayer, r), e._isBaseMap = !0, e._baseMapName = t.name, this.addLayer(e), this.fire("baseMapAdded", {
				layer: e,
				code: n,
				overlays: h ? h.length : 0
			})), e.bringToBack(), this
		},
		baseMapVisibility: function(n) {
			var u = this.MapProvider,
				e = !1,
				t, i, f, r;
			if(!n) {
				t = this._layers;
				for(i in t) t.hasOwnProperty(i) && t[i]._isBaseMap === !0 && t[i]._baseMapName === null && (this.removeLayer(t[i]), e = !0)
			}
			for(f = !1, r = 0; r < u.length; r++) u[r].selected && (f = !0), u[r].showoverlayers = n;
			return e || this.baseMap(f), this
		},
		baseMapOpacity: function(n) {
			var u = this.MapProvider,
				t = this._layers,
				i, r;
			for(i in t)
				if(t.hasOwnProperty(i) && t[i]._isBaseMap === !0) {
					if(NULL(n)) return t[i].options.opacity;
					t[i].setOpacity(n)
				}
			if(NULL(n)) return null;
			for(r = 0; r < u.length; r++) u[r].opacity = n;
			return this
		}
	});
	L.Map.include({
		bindRelation: function(n, t, i) {
			var f, r, u;
			return !NULL(i) && NULL(n.type) && (n.type = i), t = t || {}, n._bindRelation && (n._map.removeFeature(n, !0), n._bindRelation = !1), t.color && (n.color = t.color), NULL(t.rank) ? NULL(n.rank) && (n.rank = 1) : n.rank = parseInt(t.rank), NULL(t.toolTip) || (n.toolTip = t.toolTip), NULL(t.toolTipFrom) || (n.toolTipFrom = t.toolTipFrom), NULL(t.description) || (n.description = t.description), n.options.select = typeof t.select == "boolean" ? t.select : !1, f = !1, n.rank < 3 && (r = t.bindHtmlOptions || {}, n._bindhtmloptions && L.setOptions(r, n._bindhtmloptions), NULL(r.age) && (r.age = 3e3), n.bindHtml(null, r), n.bindFeatureDelete(), n.rank < 2 && (u = t.bindBubbleOptions || {}, n._bindBubbleOptions && L.setOptions(u, n._bindBubbleOptions), u.edit = n.rank < 1 ? !0 : !1, n.bindBubble(null, u), n.rank < 1 && (f = n.options.draggable = typeof t.draggable != "boolean" || t.draggable, n.bindFeatureEdit()))), f || (n.options.draggable = !1), n._bindRelation = !0, n
		},
		removeFeature: function(n, t) {
			if(n) {
				if(n.rank < 3 && (n.unbindHtml(), n.unbindFeatureDelete(), n.rank < 2 && (n.unbindBubble(), n.rank < 1 && n.unbindFeatureEdit())), t) return;
				n._map && (n._map.fire("featureRemove", {
					feature: n
				}), n._map.removeLayer(n))
			}
		},
		addPointFeature: function(n, t) {
			return t = t || {}, t.icon = this.placeMark(t), this.bindRelation(L.symbol(n, t).addTo(this), t, 0)
		},
		placeMark: function(n) {
			var i, t;
			return n = n || {}, NULL(n.icon) ? NULL(n.markerno) || typeof n.markerno == "string" && L.Util.trim(n.markerno).length === 0 || !isNaN(n.markerno) && n.markerno < 0 ? i = new L.Icon.Default(n) : (t = {}, t.markerno = n.markerno, n.iconSize && (t.iconSize = n.iconSize, t.shadowSize = n.shadowSize ? n.shadowSize : n.iconSize), n.iconAnchor && (t.iconAnchor = n.iconAnchor, t.shadowAnchor = n.shadowAnchor ? n.shadowAnchor : n.iconAnchor), t.markertype = isNaN(n.markertype) ? n.markertype = 0 : parseInt(n.markertype), t.select = typeof n.select == "boolean" ? n.select : n.select = !1, t.outline = typeof n.outline == "boolean" ? n.outline : !1, typeof n.shadow == "boolean" && (t.shadow = n.shadow), i = new L._Symbol(t)) : i = n.icon, i
		},
		addLineFeature: function(n, t) {
			var i, r;
			return t = t || {}, NULL(t.weight) && (t.weight = 1), NULL(t.opacity) && (t.opacity = 1), t.interactive = typeof t.interactive != "boolean" || t.interactive, t.draggable = typeof t.draggable == "boolean" && t.draggable, t.fill = !1, i = NULL(t.color) ? t.color = "#000000" : t.color, t.select === !0 && (i = "#ffff00"), r = t.color, t.color = i, this.bindRelation(L.polyline(n, t).addTo(this), (t.color = r, t), 1)
		},
		addPolygonFeature: function(n, t) {
			var i, r;
			return t = t || {}, NULL(t.weight) && (t.weight = 1), NULL(t.opacity) && (t.opacity = 1), t.interactive = typeof t.interactive != "boolean" || t.interactive, t.draggable = typeof t.draggable == "boolean" && t.draggable, t.fill = !0, i = NULL(t.color) ? t.color = "#000000" : t.color, t.select === !0 && (i = "#ffff00"), r = t.color, t.color = i, this.bindRelation(L.polygon(n, t).addTo(this), (t.color = r, t), 2)
		},
		addImageFeature: function(n, t) {
			var i, r, u;
			return t = t || {}, i = t.url, NULL(i) && (i = L.iPath + "logo.gif"), t.opacity = NULL(t.opacity) ? .8 : t.opacity * 1, NULL(t.angle) || (t.angle = t.angle * 1), r = typeof t.select == "boolean" ? t.select : t.select = !1, u = t.select, t.select = r, t.interactive = typeof t.interactive != "boolean" || t.interactive, t.draggable = typeof t.draggable == "boolean" && t.draggable, this.bindRelation(L.groundOverlay(i, n, t).addTo(this), (t.select = u, t), 3)
		},
		addCircleMarkerFeature: function(n, t) {
			t = t || {};
			var i = L.extend({}, t);
			return i.color = NULL(t.color) ? t.color = "#000000" : t.color, t.select === !0 && (i.color = "#ffff00"), this.bindRelation(L.circleMarker(n, i).addTo(this), t, -1)
		},
		addCircleFeature: function(n, t) {
			t = t || {};
			var i = L.extend({}, t);
			return i.color = NULL(t.color) ? t.color = "#000000" : t.color, t.select === !0 && (i.color = "#ffff00"), this.bindRelation(L.circle(n, i).addTo(this), t, -2)
		},
		addRectangleFeature: function(n, t) {
			t = t || {};
			var i = L.extend({}, t);
			return i.color = NULL(t.color) ? t.color = "#000000" : t.color, t.select === !0 && (i.color = "#ffff00"), this.bindRelation(L.rectangle(n, i).addTo(this), t, -3)
		},
		addTileOverlay: function(n) {
			var i = null,
				t;
			return n.path && (t = {}, t.minZoom = parseInt(n.minzoom) >= 0 ? parseInt(n.minzoom) : 0, t.maxZoom = parseInt(n.maxzoom) >= parseInt(n.minzoom) && parseInt(n.maxzoom) <= 100 ? parseInt(n.maxzoom) : 18, t.opacity = n.opacity && parseFloat(n.opacity) >= 0 && parseFloat(n.opacity) <= 1 ? parseFloat(n.opacity) : 1, n.detectRetina && (t.detectRetina = n.detectRetina), typeof n.errorTileUrl == "string" && (t.errorTileUrl = n.errorTileUrl), typeof n.tileSize == "number" && (t.tileSize = n.tileSize), typeof n.tms == "boolean" && (t.tms = n.tms), typeof n.subdomains == "string" && (t.subdomains = L.Util.trim(n.subdomains)), !isNaN(n.west) && n.west >= -180 && !isNaN(n.east) && n.east <= 180 && !isNaN(n.north) && n.north <= 90 && !isNaN(n.south) && n.south >= -90 && (t.bounds = L.latLngBounds([n.south, n.west], [n.north, n.east])), i = L.tileLayer(n.path, t), i._isBaseMap = null, i._baseMapName = null, this.addLayer(i), i.bringToFront(), this.fire("tileOverlayAdded", {
				layer: i
			})), i
		}
	});
	L.AnchorBlock = L.Html.extend({
		options: {
			anchor: "west",
			bordercolor: "#939da6",
			text: null,
			draggable: !1,
			propagation: !1,
			domEvents: ["click"],
			inward: !1
		},
		initialize: function(n) {
			var t, r, u, f;
			n = n || {};
			n.htmlstyle = null;
			n.followmap = !1;
			n.interactive = !0;
			n.age = 0;
			r = !0;
			switch(n.anchor) {
				case "east":
					t = "rm";
					break;
				case "south":
					t = "cb";
					r = !1;
					break;
				case "north":
					t = "ct";
					r = !1;
					break;
				default:
					t = "lm"
			}
			n.align = t;
			n.pane = "popupPane";
			n.width === i && (n.width = r ? 18 : 36);
			n.height === i && (n.height = r ? 36 : 18);
			L.setOptions(this, n);
			L.Html.prototype.initialize.call(this, null, null, this.options);
			u = this.id = "AnchorBlock_" + L.stamp(this);
			this.AnchorBlockID = u + "_Table";
			f = this.options.text;
			this.set = NULL(f) ? {
				Div: u + "_Div",
				Arrow: u + "_Arrow"
			} : null
		},
		onAdd: function(n) {
			var i = this.options,
				t;
			if(L.Html.prototype.onAdd.call(this, n), t = "openAnchorBlock", this.fire(t), n.fire(t), this.setHtmlContent(this._frame()), this.set) {
				this.on("click", this._onclick, this);
				this._onclick();
				i.draggable && (this.dragging._updatePosition = function(n) {
					this.fire("drag", n)
				})
			} else i.text.length === 0 && this.show(!1)
		},
		_dragStart: function(n) {
			this._oldPos = n.target._startPoint
		},
		_drag: function(n) {
			var r = n.target._newPos.subtract(n.target._startPos).add(n.target._startPoint),
				t, i;
			this._oldPos && (t = r.subtract(this._oldPos), i = "movedAnchorBlock", this.fire(i, {
				xy: t
			}), this._map.fire(i, {
				xy: t
			}));
			this._oldPos = r
		},
		_dragEnd: function() {},
		remove: function() {
			this._map && this._map.removeLayer(this)
		},
		onRemove: function() {
			this.set && this.off("click", this._onclick, this);
			var n = "closeAnchorBlock";
			this.fire(n);
			this._map.fire(n);
			L.Html.prototype.onRemove.call(this)
		},
		_alter: function(n, t) {
			var i;
			switch(n) {
				case "lm":
					i = "0" + (t ? "0" : "1");
					break;
				case "rm":
					i = "0" + (t ? "1" : "0");
					break;
				case "cb":
					i = "1" + (t ? "0" : "1");
					break;
				default:
					i = "1" + (t ? "1" : "0")
			}
			return i
		},
		_frame: function() {
			function u(n) {
				var t = "solid",
					i = "none";
				return n === 0 ? i + " " + t + " " + t + " " + t : n === 1 ? t + " " + i + " " + t + " " + t : n === 2 ? t + " " + t + " " + i + " " + t : t + " " + t + " " + t + " " + i
			}

			function f(n) {
				var t = "5px",
					i = "0px";
				return n === 0 ? i + " " + i + " " + t + " " + t : n === 1 ? t + " " + i + " " + i + " " + t : n === 2 ? t + " " + t + " " + i + " " + i : i + " " + t + " " + t + " " + i
			}
			var e = this.set,
				n = this.options,
				i, r, o, t;
			switch(n.align) {
				case "lm":
					i = u(3);
					r = f(3);
					break;
				case "rm":
					i = u(1);
					r = f(1);
					break;
				case "cb":
					i = u(2);
					r = f(2);
					break;
				default:
					i = u(0);
					r = f(0)
			}
			return o = "cursor:pointer;", t = [], t.push("<div " + (e ? 'id="' + e.Div + '" ' : "") + 'style="border-style:' + i + ";border-width:1px;border-color:" + n.bordercolor + ";background-color:#f4f4f4;border-radius:" + r + ";" + o + '">'), t.push('<table cellpadding="0" cellspacing="0"><tr><td style="white-space:nowrap;' + (n.width !== null ? "width:" + n.width + "px;" : "") + (n.height !== null ? "height:" + n.height + "px;" : "") + '" align="center" valign="middle" id="' + this.AnchorBlockID + '">'), t.push(e ? '<img src="" border="0" style="display:block;" id="' + e.Arrow + '"/>' : n.text), t.push("<\/td><\/tr><\/table>"), t.push("<\/div>"), t.join("")
		},
		text: function(n) {
			var t = this.options;
			return n == i ? t.text : (_(this.AnchorBlockID).innerHTML = t.text = n, this.show(n && n.length > 0), this)
		},
		change: function(n) {
			_(this.set.Arrow).src = L.iPath + "arrow/" + this._alter(this.options.align, this.options.inward = n) + ".gif"
		},
		_onclick: function(n) {
			var t = this.options,
				r, u, f;
			n !== i && (t.inward = !t.inward);
			r = t.inward;
			u = this._icon;
			t.inward && t.inwardtitle ? u.title = t.inwardtitle : !t.inward && t.outwardtitle && (u.title = t.outwardtitle);
			this.set && (this.change(r), n !== i && (f = "changedAnchorBlock", this.fire(f, {
				inward: r
			}), this._map.fire(f, {
				id: this.id,
				inward: r
			})))
		}
	});
	L.anchorBlock = function(n) {
		return new L.AnchorBlock(n)
	};
	L.Map.mergeOptions({
		zoomControl: !1
	});
	L.ZoomControl = L.Control.extend({
		options: {
			position: "topright",
			opacity: 1,
			zoomInIcon: L.iPath + "add.gif",
			zoomInTitle: "放大",
			zoomOutTitle: "缩小",
			zoomOutIcon: L.iPath + "minus.gif",
			loadingIcon: L.iPath + "waiting.gif",
			loadingTitle: "加载...",
			gpsIcon: L.iPath + "gps.gif",
			gpsTitle: "定位",
			gpsingTitle: "捕获...",
			gpsingIcon: L.iPath + "loading.gif",
			fullScreenIcon: L.iPath + "fullscreen.gif",
			fullScreenTitle: "全屏",
			fullScreen: !1,
			fullScreenContainer: null,
			fullScreenHome: null,
			zoomLevel: !0
		},
		statics: {
			TIP: null
		},
		initialize: function(n) {
			L.setOptions(this, n);
			this._Loaders = {}
		},
		_createButton: function(n, t, i, r, u, f) {
			var e = L.DomUtil.create("a", i, r),
				o;
			e.style.backgroundImage = "url('" + n + "')";
			e.href = "#";
			e.title = t;
			o = L.DomEvent.stopPropagation;
			L.DomEvent.on(e, "click", o).on(e, "mousedown", o).on(e, "dblclick", o).on(e, "click", L.DomEvent.preventDefault).on(e, "click", u, f).on(e, "click", this._refocusOnMap, f);
			return e
		},
		_zoomIn: function(n) {
			this._zoom(n, !0)
		},
		_zoomOut: function(n) {
			this._zoom(n, !1)
		},
		_zoom: function(n, t) {
			var i = this._map,
				r = i.options.zoomSnap;
			i.options.zoomSnap = .01;
			i["zoom" + (t ? "In" : "Out")](L.Browser.any3d ? n.shiftKey ? .01 : .1 : 1);
			i.options.zoomSnap = r
		},
		onAdd: function(t) {
			var i = this.options,
				u, r, f;
			if(this._map = t, t.zoomControl && t.removeControl(t.zoomControl), u = "leaflet-control-zoom", r = L.DomUtil.create("div", u + " leaflet-bar"), this._zoomInButton = this._createButton(i.zoomInIcon, i.zoomInTitle, u + "-in", r, this._zoomIn, this), i.zoomLevel) {
				this._indicator = this._createButton(i.gpsIcon, i.gpsTitle, null, r, this._locate, this);
				t.on("locationfound locationerror", this._onGPS, this);
				this._LayerListeners()
			}
			return this._MapZoom(), this._zoomlevel(), n.fullScreenApi.supportsFullScreen && i.fullScreen && (this._fullScreen = this._createButton(i.fullScreenIcon, i.fullScreenTitle, null, r, this._full, this)), this._zoomOutButton = this._createButton(i.zoomOutIcon, i.zoomOutTitle, u + "-out", r, this._zoomOut, this), f = i.opacity, r.style.filter = f >= 0 && f < 1 ? "alpha(opacity=" + (r.style.opacity = f) * 100 + ")" : r.style.opacity = "", r
		},
		onRemove: function() {
			var t = this.options;
			L.DomEvent.off(this._zoomInButton, "click", this._zoomIn, this);
			n.fullScreenApi.supportsFullScreen && t.fullScreen && L.DomEvent.off(this._fullScreen, "click", this._full, this);
			L.DomEvent.off(this._zoomOutButton, "click", this._zoomOut, this);
			this._MapZoom("off");
			t.zoomLevel && (this._LayerListeners("off"), this._map.off("locationfound locationerror", this._onGPS, this), L.DomEvent.off(this._indicator, "click", this._locate, this));
			L.ZoomControl.TIP && this._map.removeLayer(L.ZoomControl.TIP)
		},
		_onGPS: function(n) {
			var e = this.options,
				r = this._map,
				u, i;
			if(r.stopLocate(), u = this._indicator, u.title = e.gpsTitle, u.style.backgroundImage = "url('" + e.gpsIcon + "')", n.type === "locationerror") {
				switch(n.code) {
					case 0:
						i = "服务无效";
						break;
					case 1:
						i = "无权限";
						break;
					case 2:
						i = "位置无效";
						break;
					default:
						i = "超时"
				}
				this._showMsg(i)
			} else if(n.type === "locationfound") {
				var t = n.latlng,
					o = n.accuracy,
					f = 180 * o / 40075017,
					s = f / Math.cos(Math.PI / 180 * t.lat);
				r.flyToBounds(L.latLngBounds([t.lat - f, t.lng - s], [t.lat + f, t.lng + s]));
				L.bubble({
					contentwidth: 200,
					contentheight: 110
				}).setContent([{
					name: "概略位置",
					alias: "",
					content: "经度:" + L.Util.deg2dms(t.lng, 1) + "<br/>纬度:" + L.Util.deg2dms(t.lat, 1) + "<br/>精度:" + Math.ceil(o) + "m"
				}]).setLatLng(t).addTo(r)
			}
		},
		_showMsg: function(n) {
			var r = this.options,
				t = this._map,
				s = 18,
				h = L.Util.getElementPos,
				c = this._indicator,
				u = h(c).subtract(h(t.getContainer())),
				l = c.offsetWidth,
				f = "l",
				e, i, o;
			if(r.position.indexOf("left") !== -1 ? e = u.x + l + 4 : (e = -1 * (t.getSize().x - u.x + 8 + 4), f = "r"), o = "t", i = u.y + 2, r.position.indexOf("top") === -1 && (i = i + s - t.getSize().y, o = "b"), L.ZoomControl.TIP) L.ZoomControl.TIP.setHtmlContent(n);
			else {
				L.ZoomControl.TIP = L.html(null, n, {
					followmap: !1,
					age: 2e3,
					opacity: r.opacity,
					height: s,
					howfloat: f === "l" ? "left" : "right",
					align: f + o,
					dx: e,
					dy: i
				}).addTo(t);
				L.ZoomControl.TIP.once("remove", function() {
					L.ZoomControl.TIP = null
				}, this)
			}
		},
		_locate: function() {
			if(navigator.geolocation) {
				var n = this.options,
					t = this._indicator;
				t.title.indexOf(n.gpsTitle) > -1 || t.title.indexOf(n.loadingTitle) > -1 ? (this._map.locate({
					watch: !1,
					setView: !1,
					enableHighAccuracy: !0
				}), t.title = n.gpsingTitle, t.style.backgroundImage = "url('" + n.gpsingIcon + "')") : t.title.indexOf(n.gpsingTitle) > -1 && this._onGPS({
					type: null
				})
			} else this._showMsg("无服务")
		},
		_full: function() {
			var i = this.options,
				u = i.fullScreenContainer || this._map._container,
				r = i.fullScreenHome,
				t = n.fullScreenApi;
			t.isFullScreen(r) ? t.cancelFullScreen(r) : t.requestFullScreen(u)
		},
		_updateIndicator: function() {
			var i = 0,
				r, n, t;
			for(r in this._Loaders) this._Loaders.hasOwnProperty(r) && i++;
			n = this.options;
			t = this._indicator;
			t.title.indexOf(n.gpsingTitle) === -1 && (t.title = i > 0 ? n.loadingTitle : n.gpsTitle, t.style.backgroundImage = i > 0 ? "url('" + n.loadingIcon + "')" : "url('" + n.gpsIcon + "')")
		},
		_handleLoading: function(n) {
			this._Loaders[this._eventId(n)] = !0;
			this._updateIndicator()
		},
		_handleLoad: function(n) {
			delete this._Loaders[this._eventId(n)];
			this._updateIndicator()
		},
		_eventId: function(n) {
			return n.id ? n.id : n.layer ? n.layer._leaflet_id : n.target._leaflet_id
		},
		_layerAdd: function(n) {
			if(n.layer && n.layer.on) n.layer.on({
				loading: this._handleLoading,
				load: this._handleLoad
			}, this)
		},
		_LayerListeners: function(n) {
			n = n || "on";
			this._map.eachLayer(function(t) {
				t[n] && t[n]({
					loading: this._handleLoading,
					load: this._handleLoad
				}, this)
			}, this);
			this._map[n]("layeradd", this._layerAdd, this)
		},
		_zoomlevel: function() {
			var n = setInterval(L.bind(function() {
				var t, i, r;
				this._zoomInButton && this._zoomOutButton && (clearInterval(n), t = this._map, i = "leaflet-disabled", L.DomUtil.removeClass(this._zoomInButton, i), L.DomUtil.removeClass(this._zoomOutButton, i), (this._disabled || t._zoom === t.getMinZoom()) && L.DomUtil.addClass(this._zoomOutButton, i), (this._disabled || t._zoom === t.getMaxZoom()) && L.DomUtil.addClass(this._zoomInButton, i), this.options.zoomLevel && (r = t.getZoom(), this._showMsg((Math.floor(r) === Math.ceil(r) ? r : r.toFixed(2)) + " / " + t.getMaxZoom())))
			}, this), 5)
		},
		_MapZoom: function(n) {
			this._map[n || "on"]("zoomend zoomlevelschange", this._zoomlevel, this)
		}
	});
	L.zoomControl = function(n) {
		return new L.ZoomControl(n)
	};
	L.ScaleControl = L.Control.extend({
		options: {
			position: "bottomleft",
			maxWidth: 100,
			outcolor: "#ffffff",
			incolor: "#000000",
			outline: "#808080",
			size: "11",
			opacity: 1,
			title: null
		},
		onAdd: function(n) {
			var f;
			this._map = n;
			this.id = "L_ScaleControl_" + L.stamp(this);
			var i = L.DomUtil.create("div"),
				r = i.style,
				t = this.options,
				u = t.opacity;
			return u >= 0 && u < 1 ? (r.filter = "alpha(opacity=" + u * 100 + ")", r.opacity = u) : r.filter = r.opacity = "", f = t.title, f && (i.title = f), this._mScale = L.Util.innerhtml2dom(L.DomUtil.create("div", null, i), '<table style="width:100%;height:100%;"><tr><td style="border:1px solid ' + t.outcolor + ";width:1px;background-color:" + t.incolor + ';"><\/td><td><table><tr><td style="width:50%;"><table style="width:100%;"><tr><td><\/td><td style="border-style:solid none solid none;border-width:1px;border-color:' + t.outcolor + ";height:1px;background-color:" + t.incolor + ';width:100%;"><\/td><td><\/td><\/tr><\/table><\/td><td><div id="' + this.id + '" style="border:1px solid ' + t.outline + ";text-align:center;vertical-align:middle;padding:0px 2px 0px 2px;background-color:" + t.outcolor + ";white-space:nowrap;font-size:" + parseInt(t.size) + "px;border-radius:4px;color:" + t.incolor + ';"><\/div><\/td><td style="width:50%;"><table style="width:100%;"><tr><td><\/td><td style="border-style:solid none solid none;border-width:1px;border-color:' + t.outcolor + ";height:1px;background-color:" + t.incolor + ';width:100%;"><\/td><td><\/td><\/tr><\/table><\/td><\/tr><\/table><\/td><td style="border:1px solid ' + t.outcolor + ";width:1px;background-color:" + t.incolor + ';"><\/td><\/tr><\/table>'), this._Events(), n.whenReady(this._update, this), i
		},
		onRemove: function() {
			this._Events("off")
		},
		_Events: function(n) {
			this._map[n || "on"]("moveend", this._update, this)
		},
		_update: function() {
			var t = this._map,
				i = t.getSize().x / 2,
				r = t.getSize().y / 2,
				n = this.options.maxWidth / 2,
				u = L.Util.distance(t.containerPointToLatLng([i > n ? i - n : 0, r]), t.containerPointToLatLng([i > n ? i + n : n * 2, r]));
			this._updateMetric(u)
		},
		_updateMetric: function(n) {
			var f = Math.pow(10, (Math.floor(n) + "").length - 1),
				t = n / f,
				i, r, u, e;
			t = t >= 10 ? 10 : t >= 5 ? 5 : t >= 3 ? 3 : t >= 2 ? 2 : 1;
			i = f * t;
			this._mScale.style.width = Math.round(this.options.maxWidth * (i / n)) + 2 + "px";
			r = _(this.id);
			u = function() {
				r.innerHTML = i < 1e3 ? i + "m" : i / 1e3 + "km"
			};
			r ? u.call(this) : e = setInterval(L.bind(function() {
				r = _(this.id);
				r && (clearInterval(e), u.call(this))
			}, this), 5)
		}
	});
	L.scaleControl = function(n) {
		return new L.ScaleControl(n)
	};
	L.Map.mergeOptions({
		attributionControl: !1
	});
	L.CornerControl = L.Control.extend({
		options: {
			position: "bottomright",
			closetitle: "隐藏",
			closeshow: !0,
			outline: !1,
			outlinecolor: "#939da6",
			padding: "2px",
			width: null,
			height: null,
			backgroundcolor: null,
			text: null,
			opacity: .7
		},
		initialize: function(n) {
			L.setOptions(this, n);
			this._attributions = {}
		},
		onAdd: function(n) {
			var s, u, t, i, h, c, r, f, e, l, o, a;
			if(this._map = n, s = this._container = L.DomUtil.create("div"), L.DomEvent.disableClickPropagation(s), u = this.options.opacity, t = s.style, t.marginTop = t.marginBottom = t.marginRight = t.marginLeft = 0, u >= 0 && u < 1 ? (t.filter = "alpha(opacity=" + u * 100 + ")", t.opacity = u) : t.filter = t.opacity = "", i = this.options.backgroundcolor, NULL(i) || (t.backgroundColor = i), h = this.options.text, NULL(h) || L.Util.trim(h).length === 0)
				for(c in n._layers) n._layers.hasOwnProperty(c) && n._layers[c].getAttribution && this._add(n._layers[c].getAttribution());
			else this._add(h, !0);
			if(this.left = !1, this.options.outline) {
				t.padding = this.options.padding;
				t.borderWidth = "1px";
				t.borderColor = this.options.outlinecolor;
				switch(this.options.position) {
					case "topleft":
						t.borderStyle = "none solid solid none";
						t.borderRadius = "0 0 5px 0";
						break;
					case "topright":
						t.borderStyle = "none none solid solid";
						t.borderRadius = "0 0 0 5px";
						break;
					case "bottomleft":
						t.borderStyle = "solid solid none none";
						t.borderRadius = "0 5px 0 0";
						break;
					default:
						t.borderStyle = "solid none none solid";
						t.borderRadius = "5px 0 0 0"
				}
			}
			if(this.options.closeshow) {
				switch(this.options.position) {
					case "topleft":
						e = "◤";
						this.left = !0;
						f = 0;
						break;
					case "topright":
						e = "◥";
						f = 0;
						break;
					case "bottomleft":
						e = "◣";
						this.left = !0;
						f = 1;
						break;
					default:
						e = "◢";
						f = 1
				}
				r = this.hide = L.DomUtil.create("div");
				l = r.style;
				l.cursor = "pointer";
				l.verticalAlign = f === 0 ? "top" : "bottom";
				l.display = "table-cell";
				r.title = this.options.closetitle;
				r.innerHTML = e
			}
			return o = this.copyright = L.DomUtil.create("div"), a = o.style, i = this.options.width, NULL(i) || (a.width = i), i = this.options.height, NULL(i) || (a.height = i), a.display = "table-cell", this.options.closeshow ? (this._container.appendChild(this.left ? r : o), this._container.appendChild(this.left ? o : r)) : this._container.appendChild(o), this._fireevent(), n.whenReady(this._update, this), s
		},
		onhideclick: function() {
			this._map.fire("cornerRemoved");
			this.remove()
		},
		_fireevent: function(n) {
			n = n || "on";
			var t = this.options.text;
			(NULL(t) || L.Util.trim(t).length === 0) && this._map[n]({
				baseMapAdded: this._onLayerAdd,
				baseMapDeleted: this._onLayerRemove
			}, this);
			this.options.closeshow && L.DomEvent[n](this.hide, "click", this.onhideclick, this)
		},
		onRemove: function() {
			this._fireevent("off")
		},
		_add: function(n, t) {
			return t && (this._attributions = {}), n && (this._attributions[n] || (this._attributions[n] = 0), this._attributions[n]++, this._update()), this
		},
		_remove: function(n) {
			return n && this._attributions[n] && (this._attributions[n]--, this._update()), this
		},
		_onLayerAdd: function(n) {
			n.layer.getAttribution && this._add(n.layer.getAttribution())
		},
		_onLayerRemove: function(n) {
			n.layer.getAttribution && this._remove(n.layer.getAttribution())
		},
		_update: function() {
			var n = function() {
					var n = [],
						t = this._attributions;
					for(var i in t) t.hasOwnProperty(i) && t[i] && n.push(i);
					this._container.style.display = n.length ? (this.copyright.innerHTML = n.join(" | "), "block") : "none"
				},
				t;
			this._container && this.copyright ? n.call(this) : t = setInterval(L.bind(function() {
				this._container && this.copyright && (clearInterval(t), n.call(this))
			}, this), 5)
		}
	});
	L.cornerControl = function(n) {
		return new L.CornerControl(n)
	};
	L.MapGrid = L.Layer.extend({
		options: {
			labelled: !1,
			scalecode: null,
			linecolor: "#939da6",
			linewidth: 1,
			dashArray: "10,1",
			labelfontcolor: "#ffffff",
			labelfontsize: 10,
			labelbackcolor: "#939da6",
			opacity: 1
		},
		initialize: function(n) {
			L.setOptions(this, n);
			this.scalestring = null;
			this._gridarray = [];
			this._maplevel = this.options.scalecode
		},
		label: function(n) {
			typeof n != "boolean" && (n = !0);
			this.options.labelled = n;
			this._drawgrid()
		},
		scale: function(n) {
			this._drawgrid(typeof n == "number" ? n < 0 ? -1 : n + 6 : null);
			this._map && this._map.fire("mapGridAdded", {
				status: this.scalestring
			})
		},
		onAdd: function(n) {
			this._map = n;
			n.on({
				moveend: this._drawgrid
			}, this);
			return this._drawgrid(), n.fire("mapGridAdded", {
				status: this.scalestring
			}), this
		},
		onRemove: function() {
			for(var n = 0; n < this._gridarray.length; n++) this._map.removeLayer(this._gridarray[n]);
			this._gridarray = null;
			this._map.off({
				moveend: this._drawgrid
			}, this);
			this._map.fire("mapGridRemoved")
		},
		_gridscale: function(n) {
			var t;
			return t = n >= 13 ? {
				db: 3 / 144,
				dl: .03125,
				flag: 4,
				status: "5"
			} : n >= 12 ? {
				db: 1 / 24,
				dl: 1 / 16,
				flag: 4,
				status: "10"
			} : n >= 11 ? {
				db: 4 / 48,
				dl: 6 / 48,
				flag: 4,
				status: "25"
			} : n >= 10 ? {
				db: 4 / 24,
				dl: 6 / 24,
				flag: 4,
				status: "50"
			} : n >= 9 ? {
				db: 4 / 12,
				dl: 6 / 12,
				flag: 2,
				status: "100"
			} : n >= 8 ? {
				db: 4 / 6,
				dl: 1,
				flag: 2,
				status: "200"
			} : n >= 7 ? {
				db: 1,
				dl: 1.5,
				flag: 2,
				status: "250"
			} : n >= 6 ? {
				db: 2,
				dl: 3,
				flag: 0,
				status: "500"
			} : n >= 2 ? {
				db: 4,
				dl: 6,
				flag: 0,
				status: "1000"
			} : {
				db: 0,
				dl: 0,
				flag: 0,
				status: null
			}, t.status && (t.status = "1:" + t.status + "000"), this.scalestring = t.status, t
		},
		_drawgrid: function(n) {
			var t, i;
			this._map && (typeof n == "number" ? n >= 0 && n <= this._map.getMaxZoom() ? t = this._maplevel = n : (this._maplevel = null, t = this._map.getZoom()) : t = this._maplevel == null ? this._map.getZoom() : this._maplevel, i = this._gridscale(t), this.showing && clearTimeout(this.showing), this.showing = setTimeout(L.bind(function() {
				var f = this._map.getBounds(),
					o, u;
				if(f) {
					for(o = 0; o < this._gridarray.length; o++) this._map.removeLayer(this._gridarray[o]);
					this._gridarray = [];
					var e = f.getWest(),
						n = f.getSouth(),
						s = f.getNorth(),
						h = f.getEast(),
						t = i.db,
						r = i.dl;
					if(t * r != 0) {
						for(e = Math.floor(e / r) * r, n = Math.floor(n / t) * t, s = Math.ceil(s / t) * t, h = Math.ceil(h / r) * r, u = e; u <= h;) this._drawline([n, u], [s, u]), this.options.labelled && this._drawlabel([n + t, u], this._createlabel(u, !1), "lt"), u += r;
						while(n <= s) this._drawline([n, e], [n, h]), this.options.labelled && this._drawlabel([n, e + r], this._createlabel(n, !0), "lb"), n += t
					}
				}
			}, this), 0))
		},
		_createlabel: function(n, t) {
			return t || (n = L.Util.longitude(n)), (t ? n > 0 ? "N" : n < 0 ? "S" : "" : n > 0 && n !== 180 ? "E" : n < 0 && n !== -180 ? "W" : "") + L.Util.deg2dms(Math.abs(n))
		},
		_drawline: function(n, t) {
			var i = this.options;
			this._gridarray.push(L.polyline([n, t], {
				color: i.linecolor,
				weight: i.linewidth,
				dashArray: i.dashArray,
				opacity: i.opacity,
				interactive: !1
			}).addTo(this._map))
		},
		_drawlabel: function(n, t, i) {
			var r = this.options;
			this._gridarray.push(L.html(L.latLng(n), t, {
				pane: "overlayPane",
				htmlstyle: "font-size:" + parseInt(r.labelfontsize) + "px;color:" + r.labelfontcolor + ";background-color:" + r.labelbackcolor + ";white-space:nowrap;",
				opacity: r.opacity,
				align: i,
				fadeable: !1,
				interactive: !1
			}).addTo(this._map))
		}
	});
	L.mapGrid = function(n) {
		return new L.MapGrid(n)
	};
	L.BoxSelect = L.Handler.extend({
		options: {
			action: 0,
			close: !0
		},
		initialize: function(n, t) {
			this._map = n;
			this._pane = n._panes.overlayPane;
			this._moved = !1;
			t = t || {};
			L.setOptions(this, t)
		},
		addHooks: function() {
			var n = this._map._container;
			n.tabIndex === -1 && (n.tabIndex = "0");
			this._DomDown();
			n.focus()
		},
		removeHooks: function() {
			this._DomDown("off");
			this._moved = !1
		},
		_DomDown: function(n) {
			var t = this._map._container;
			L.DomEvent[n || "on"](t, "mousedown " + L.DOMtrigger.DOMevents[6], this._onMouseDown, this)[n || "on"](t, "keydown", this._onKeyDown, this)
		},
		_onKeyDown: function(n) {
			n.keyCode === 27 && this._finish(!1)
		},
		moved: function() {
			return this._moved
		},
		enable: function() {
			if(!this._enabled) {
				var n = this._map;
				n.boxZoom && n.boxZoom.disable();
				n.dragging && n.dragging.disable();
				n.doubleClickZoom && n.doubleClickZoom.disable();
				n.touchZoom && n.touchZoom.disable();
				n.tap && n.tap.disable();
				L.Handler.prototype.enable.call(this);
				L.DomUtil.disableTextSelection();
				L.DomUtil.disableImageDrag()
			}
		},
		disable: function() {
			if(this._enabled) {
				var n = this._map;
				n.boxZoom && n.boxZoom.enable();
				n.dragging && n.dragging.enable();
				n.doubleClickZoom && n.doubleClickZoom.enable();
				n.touchZoom && n.touchZoom.enable();
				n.tap && n.tap.enable();
				L.Handler.prototype.disable.call(this);
				L.DomUtil.enableTextSelection();
				L.DomUtil.enableImageDrag()
			}
		},
		_DomDownUp: function(n) {
			L.DomEvent[n || "on"](t, "mousemove " + L.DOMtrigger.DOMevents[8], this._onMouseMove, this)[n || "on"](t, "mouseup " + L.DOMtrigger.DOMevents[10], this._onMouseUp, this)
		},
		_onMouseDown: function(n) {
			this._moved = !1;
			this._assistkey = n.shiftKey ? 1 : n.ctrlKey ? 2 : n.altKey ? 3 : null;
			this._endPoint = this._startPoint = this._map.mouseEventToLayerPoint(n.touches && n.touches.length > 0 ? n.touches[0] : n);
			this._DomDownUp()
		},
		_onMouseMove: function(n) {
			var t = this._map,
				i, r, u, f, e;
			L.DomEvent.preventDefault(n);
			i = this._startPoint;
			this._moved || (this._moved = !0, this._box = L.DomUtil.create("div", "leaflet-zoom-box", this._pane), L.DomUtil.setPosition(this._box, i), t.fire("boxSelectStart", {
				point: t.layerPointToLatLng(i),
				options: this.options
			}));
			r = this._endPoint = t.mouseEventToLayerPoint(n.touches && n.touches.length > 0 ? n.touches[0] : n);
			t.fire("boxSelectMove", {
				point: t.layerPointToLatLng(r),
				options: this.options
			});
			u = new L.Bounds(r, i);
			f = u.getSize();
			L.DomUtil.setPosition(this._box, u.min);
			e = this._box.style;
			e.width = f.x + "px";
			e.height = f.y + "px"
		},
		_onMouseUp: function() {
			this._DomDownUp("off");
			this._finish(!0)
		},
		_finish: function(n) {
			this._moved && this._pane.removeChild(this._box);
			var t = this._map;
			t.fire("boxSelectEnd", {
				box: n ? new L.LatLngBounds(t.layerPointToLatLng(this._startPoint), t.layerPointToLatLng(this._endPoint)) : null,
				code: this._assistkey,
				options: this.options
			});
			this.options.close && this.disable()
		}
	});
	L.MeasureTool = L.Class.extend({
		statics: {
			THIS: null
		},
		options: {
			howmanynodes: 100,
			markerno: -1,
			markertype: null,
			closed: !1,
			color: "#ff0000",
			weight: 5,
			opacity: 1,
			lineCap: null,
			lineJoin: null,
			dashArray: "10,8",
			fill: !0,
			fillColor: null,
			fillOpacity: .2
		},
		initialize: function(n, t) {
			this._map = n;
			t = t || {};
			t.draggable = !0;
			t.rank = null;
			L.Util.setOptions(this, t);
			var i = L.MeasureTool.THIS;
			i && i.disable();
			L.MeasureTool.THIS = this
		},
		enable: function(n) {
			if(!this._enabled) {
				this._enabled = !0;
				typeof n == "boolean" && (this.options.closed = n);
				this.nodes = [];
				this.geometry = null;
				var t = this._map;
				t.on("click", this._addNode, this).on("contextmenu", this._fireStop, this);
				t.fire("measureStart")
			}
		},
		enabled: function() {
			return !!this._enabled
		},
		_fireStop: function() {
			this._map.fire("measureStop", {
				vertex: this._getline()
			})
		},
		disable: function() {
			var n, t;
			if(this._enabled) {
				for(n = this._map, n.fire("measureEnd", {
						vertex: this._getline()
					}), this._enabled = !1; this.nodes && this.nodes.length > 0;) t = this.nodes.pop(), t.off("drag", this._draw, this).off("click", this._removeNode, this), n.removeFeature(t);
				this.nodes = null;
				this.geometry && n.removeFeature(this.geometry);
				this.geometry = null;
				n.off("click", this._addNode, this).off("contextmenu", this._fireStop, this)
			}
		},
		_addNode: function(n) {
			var i = this.options.howmanynodes,
				t;
			if(this.nodes.length > i) {
				alert("提示：点数不应超过" + i + "个！");
				return
			}
			L.DomEvent.stop(n);
			t = this._map.addPointFeature(n.latlng, this.options);
			t.on("drag", this._draw, this).on("click", this._removeNode, this);
			this.nodes.push(t);
			this._draw()
		},
		_getline: function() {
			for(var n = [], t = 0, i = this.nodes.length; t < i; t++) n.push(this.nodes[t].getLatLng());
			return this.options.closed && this.nodes[0] && n.push(this.nodes[0].getLatLng()), n
		},
		_removeNode: function(n) {
			var u, t, i, f, r;
			for(L.DomEvent.stop(n), u = n.target._leaflet_id, t = -1, i = 0, f = this.nodes.length; i < f; i++)
				if(this.nodes[i]._leaflet_id === u) {
					t = i;
					break
				}
			t > -1 && (r = this.nodes[t], r.off("drag", this._draw, this).off("click", this._removeNode, this), this._map.removeFeature(r), this.nodes.splice(t, 1), this._draw())
		},
		_draw: function() {
			var n = this._getline(),
				t = this._map,
				i;
			this.nodes.length < 2 ? this.geometry && (t.removeFeature(this.geometry), this.geometry = null) : (i = this.options.closed, this.geometry ? this.geometry.setLatLngs(i ? [n] : n) : this.geometry = i ? t.addPolygonFeature([n], this.options) : t.addLineFeature(n, this.options));
			t.fire("measuring", {
				vertex: n
			})
		}
	});
	L.PagesBox = L.Html.extend({
		options: {
			total: 0,
			many: null,
			tip: null,
			title: null,
			width: 340,
			height: 62,
			draggable: !0,
			domEvents: [],
			buttons: 10,
			page: 1,
			align: "lt",
			dx: 10,
			dy: 10,
			job: null
		},
		statics: {
			DX: null,
			DY: null,
			THIS: null
		},
		initialize: function(n) {
			var i, t;
			L.PagesBox.THIS && L.PagesBox.THIS.remove();
			L.PagesBox.THIS = this;
			n = n || {};
			n.htmlstyle = null;
			n.followmap = !1;
			n.interactive = !0;
			n.age = 0;
			n.pane = "popupPane";
			L.PagesBox.DX !== null && L.PagesBox.DY !== null && (n.dx = L.PagesBox.DX, n.dy = L.PagesBox.DY);
			isNaN(n.many) && (n.many = 10);
			(n.page < 1 || n.page > Math.ceil(n.total / n.many)) && (n.page = 1);
			n.button = 1;
			L.setOptions(this, n);
			i = this.options;
			L.Html.prototype.initialize.call(this, null, null, i);
			t = this.id = "PagesBox_" + L.stamp(this);
			this.set = {
				Head: t + "_Head",
				Container: t + "_Container",
				Before: t + "_Before",
				Buttons: function() {
					for(var r = [], n = 0; n < i.buttons; n++) r.push(t + "_Button" + n);
					return r
				}.call(this),
				After: t + "_After"
			}
		},
		remove: function() {
			this._map && this._map.removeLayer(this)
		},
		onAdd: function(n) {
			L.Html.prototype.onAdd.call(this, n);
			var t = "openPagesBox";
			this.fire(t);
			n.fire(t);
			this.setHtmlContent(this._frame()).head().tip()._buttons().page();
			this._eventer()
		},
		onRemove: function() {
			var t, n;
			this._eventer("off");
			t = "closePagesBox";
			this.fire(t);
			this._map.fire(t);
			L.Html.prototype.onRemove.call(this);
			n = _(this.id + "_div");
			n && n.parentNode.removeChild(n);
			L.PagesBox.THIS = null
		},
		_eventer: function(n) {
			n = n || "on";
			L.PagesBox.THIS[n]({
				Exit: this._onexit,
				Before: this._before,
				After: this._after
			}, this);
			this[n]("htmlMoveEnd", this._onmoveend, this)
		},
		_onmoveend: function(n) {
			L.PagesBox.DX = n.dx;
			L.PagesBox.DY = n.dy
		},
		_frame: function() {
			var r = this.options,
				t = this.set,
				n = [],
				i;
			for(n.push('<div style="border:1px solid #999999;width:100%;background-color:#f4f4f4;border-radius:4px;box-shadow:0 1px 5px rgba(0,0,0,0.65);font-size:12px;font-family:Verdana,Geneva,Tahoma,sans-serif;">'), n.push('<table cellpadding="0" cellspacing="0" style="width:100%"><tr><td>'), n.push('<table cellpadding="4" cellspacing="0" style="border-spacing:2px; border-style:none none solid none;border-width:1px;border-color:#cccccc;width:100%"><tr><td style="padding:2px"><table cellpadding="0" cellspacing="0" style="width:100%"><tr>'), n.push('<td align="center" style="cursor:default;padding:2px;overflow-x:auto;-webkit-overflow-scrolling:touch;" id="' + t.Head + '">&nbsp;<\/td>'), n.push('<td align="right" style="padding:2px;cursor:pointer;width:18px;vertical-align:middle;" title="关闭" onclick="L.PagesBox.THIS.fire(\'Exit\');"><img alt="" src="' + L.iPath + 'delete.gif" style="display:block" /><\/td><\/tr><\/table><\/td><\/tr><\/table><\/td><\/tr><tr><td>'), n.push('<table cellpadding="4" cellspacing="0" style="border-spacing:2px;width:100%"><tr><td style="padding:4px;border-right-style:solid;border-width:1px;border-color:#cccccc;width:18px;"><img id="' + t.Before + '" alt="" src="' + L.iPath + 'backward.gif" title="后退" style="cursor:pointer;display:block" onclick="L.PagesBox.THIS.fire(\'Before\');"/><\/td>'), n.push('<td style="padding:2px;background-color:#e6e7e8;" id="' + t.Container + '">'), n.push('<div style="width:' + (r.width - 56) + 'px;overflow-x:auto;-webkit-overflow-scrolling:touch;overflow-y:hidden;">'), n.push('<table style="border-collapse:separate;border-spacing:1px;width:100%;height:22px;font-family:Verdana,Geneva,Tahoma,sans-serif;font-size:9px"><tr>'), i = 0; i < r.buttons; i++) n.push('<td align="center" class="buttonboxstyle" id="' + t.Buttons[i] + '" onmouseover="L.PagesBox.THIS._over(this);" onmouseout="L.PagesBox.THIS._out(this);" onclick="L.PagesBox.THIS._down(this);L.PagesBox.THIS._up(this);">&nbsp;<\/td>');
			return n.push("<\/tr><\/table><\/div>"), n.push('<\/td><td align="right" style="padding:4px;border-left-style:solid;border-width:1px;border-color:#cccccc;width:18px;"><img id="' + t.After + '" title="前进" alt="" src="' + L.iPath + 'forward.gif" style="cursor:pointer;display:block" onclick="L.PagesBox.THIS.fire(\'After\');"/><\/td><\/tr><\/table><\/td><\/tr><\/table>'), n.push("<\/div>"), n.join("")
		},
		_buttons: function() {
			var n = this.options,
				u = Math.ceil(n.total / n.many),
				f = Math.ceil(u / n.buttons),
				i, t, r;
			for(n.button > f ? n.button = 1 : n.button < 1 && (n.button = f), i = this.set, t = 0; t < n.buttons; t++) r = (n.button - 1) * n.buttons + t + 1, _(i.Buttons[t]).innerHTML = r <= u ? r : "&nbsp;", _(i.Buttons[t]).className = r <= u ? r === n.page ? "buttonselectstyle" : "buttonstyle" : "buttonnonestyle";
			return _(i.Before).style.display = _(i.After).style.display = f > 1 ? "block" : "none", this
		},
		_onexit: function(n) {
			n && L.DomEvent.stop(n);
			this._map.removeLayer(this)
		},
		_before: function() {
			--this.options.button;
			this._buttons()
		},
		_after: function() {
			++this.options.button;
			this._buttons()
		},
		_over: function(n) {
			n.className === "buttonstyle" && (n.className = "buttonoverstyle")
		},
		_out: function(n) {
			(n.className === "buttonoverstyle" || n.className === "buttondownstyle") && (n.className = "buttonstyle")
		},
		_down: function(n) {
			(n.className === "buttonoverstyle" || n.className === "buttonstyle") && (n.className = "buttondownstyle")
		},
		_up: function(n) {
			n.ispagebuttonclicked && clearTimeout(n.ispagebuttonclicked);
			n.ispagebuttonclicked = setTimeout(function(t) {
				return function() {
					var i, r;
					if(clearTimeout(n.ispagebuttonclicked), n.ispagebuttonclicked = null, n.className === "buttondownstyle" || n.className === "buttonstyle" || n.className === "buttonoverstyle") {
						for(i = 0; i < t.options.buttons; i++)
							if(r = _(t.set.Buttons[i]), r !== n && "buttonselectstyle" === r.className) {
								r.className = "buttonstyle";
								break
							}
						n.className = "buttonselectstyle";
						t.options.page = parseInt(n.innerHTML);
						t.page()
					}
				}
			}(this), 100)
		},
		total: function(n, t) {
			var i = this.options;
			return typeof n == "number" && n > 0 && n !== i.total && (i.total = n, i.page = 1, this.head()._buttons(), t && this.page()), this
		},
		many: function(n, t) {
			var i = this.options;
			return typeof n == "number" && n > 0 && n !== i.many && (i.many = n, i.page = 1, this.head()._buttons(), t && this.page()), this
		},
		head: function(n) {
			var i = _(this.set.Head),
				t = this.options;
			return i && (i.innerHTML = NULL(n) ? "共" + t.total + "个 分" + Math.ceil(t.total / t.many) + "页" : n), this
		},
		tip: function(n) {
			var t = this.options;
			return n !== i && (t.tip = n), _(this.set.Head).title = NULL(t.tip) ? "" : t.tip, this
		},
		page: function(n) {
			var t = this.options,
				i, r;
			return NULL(n) ? n = t.page : ((n < 1 || n > Math.ceil(t.total / t.many)) && (n = 1), t.page = n, this._buttons()), i = {
				page: n,
				many: t.many,
				total: t.total,
				job: t.job
			}, r = "doPagesBox", this.fire(r, i), this._map.fire(r, i), this
		}
	});
	L.pagesBox = function(n) {
		return new L.PagesBox(n)
	};
	L.Handler._NodeDrag = L.Handler.MarkerDrag.extend({
		addHooks: function() {
			var n = this._marker._icon;
			this._draggable || (this._draggable = new L.Draggable(n));
			this._draggable._onDown = function(n) {
				if(!n._simulated && this._enabled && (this._moved = !1, !L.DomUtil.hasClass(this._element, "leaflet-zoom-anim")) && (n.shiftKey || n.which !== 1 && n.button !== 1 && !n.touches)) {
					if(L.DomEvent.stopPropagation(n), L.Draggable._dragging = !0, L.DomUtil.disableImageDrag(), L.DomUtil.disableTextSelection(), this._moving) return;
					this.fire("down");
					var i = n.touches ? n.touches[0] : n;
					this._startPoint = L.point(i.clientX, i.clientY);
					this._startPos = this._newPos = L.DomUtil.getPosition(this._element);
					L.DomEvent.on(t, L.Draggable.MOVE[n.type], this._onMove, this).on(t, L.Draggable.END[n.type], this._onUp, this)
				}
			};
			this._draggable.on({
				dragstart: this._onDragStart,
				drag: this._onDrag,
				dragend: this._onDragEnd
			}, this).enable();
			L.DomUtil.addClass(n, "leaflet-marker-draggable")
		}
	});
	L.DrawPad = L.Handler.extend({
		statics: {
			THIS: null
		},
		options: {
			keep: !0,
			rank: 0,
			draggable: !0,
			interactive: !0,
			propagation: !1,
			select: !1,
			toolTip: "",
			toolTipFrom: "-1"
		},
		type: null,
		dirty: !1,
		streamMode: !0,
		outline: !0,
		nodeoptions: {
			rank: 2,
			title: null,
			alt: null,
			toolTipFrom: null,
			description: null,
			keyboard: !1,
			select: !1,
			interactive: !0,
			draggable: !1,
			propagation: !0,
			toolTip: "左键：定点<br/>右键：结束<br/>技巧：Shift",
			opacity: .7,
			bindHtmlOptions: {
				age: 0,
				dx: 18,
				dy: 0,
				opacity: .6,
				fadeable: !1
			}
		},
		initialize: function(n, t) {
			var i = L.DrawPad.THIS;
			i && i.disable();
			L.DrawPad.THIS = this;
			this._map = n;
			L.setOptions(this, t);
			this.toolTipEnable = !0
		},
		addHooks: function() {
			var n = this._map;
			n && (this.nodes = 0, n.webMapping = !0, n.fire("featureDrawStart", {
				featuretype: this.type
			}), L.DomUtil.disableTextSelection(), L.DomUtil.disableImageDrag(), n.getContainer().focus(), this._listenWebMapping())
		},
		_listenWebMapping: function(n) {
			n = n || "on";
			this._map[n]({
				mouseover: this._anchor,
				mouseout: this._anchor,
				mousemove: this._onMapMouseMove,
				contextmenu: this._cancelDrawing,
				click: this._onMapClick
			}, this)
		},
		removeHooks: function() {
			var n = this._map;
			n && (n.webMapping = !1, this._listenWebMapping("off"), L.DomUtil.enableTextSelection(), L.DomUtil.enableImageDrag(), delete this.nodeoptions, this.node && (this._listenNode("off"), n.removeFeature(this.node), this.node = null), n.fire("featureDrawStop", {
				featuretype: this.type
			}))
		},
		_listenNode: function(n) {
			n = n || "on";
			this.node[n]("contextmenu", this._cancelDrawing, this);
			this.streamMode && (this.node[n]("dragstart", this._DragStart, this)[n]("drag", this._Drag, this)[n]("dragend", this._DragEnd, this), n === "on" ? (this.nodedragging = new L.Handler._NodeDrag(this.node), this.nodedragging.enable()) : (this.nodedragging.disable(), this.nodedragging = null))
		},
		_onMapMouseMove: function(n) {
			var t = this.node;
			t && (t.setLatLng(n.latlng), t._bindHtmlObject && t._bindHtmlObject.bringToFront())
		},
		nodeIcon: function(n) {
			var i = null,
				t = this.node;
			return n ? (L.extend(this.nodeoptions, n), t && t.setIcon(this._map.placeMark(this.nodeoptions))) : t && (i = t.options.icon), i
		},
		_cancelDrawing: function(n) {
			n.type === "contextmenu" && (this.dirty ? this._redowork() : (this.node && this._map.fire("featureDrawCancel", {
				featuretype: this.type
			}), this.disable()))
		},
		_createAnchor: function(n) {
			NULL(this.node) && (this.node = this._map.addPointFeature(n, this.nodeoptions), this.outline && (this.node._icon.style.outline = (this.guidelineoptions ? this.guidelineoptions.color : "#000000") + " dashed 1px"), this._listenNode());
			this.node._bindHtmlObject && (this.nodes < 1 ? this.node._bindHtmlObject.setHtmlOpacity(this.nodeoptions.bindHtmlOptions.opacity) : (this.node.unbindHtml(), this.node._bindHtmlObject = null));
			this.node.setOpacity(this.nodeoptions.opacity)
		},
		_anchor: function(n) {
			if(n.type === "mouseover") this._createAnchor(n.latlng);
			else {
				var t = this.node;
				t && (t._bindHtmlObject && t._bindHtmlObject.setHtmlOpacity(0), t.setOpacity(0))
			}
		},
		_DragStart: function(n) {
			this._onMapClick(n);
			this.firstnode = this._map.latLngToContainerPoint(this.node.getLatLng());
			this.secondnode = null
		},
		_Drag: function(n) {
			var u = this.node,
				f;
			this.secondnode = this._map.latLngToContainerPoint(u.getLatLng());
			var e = this.secondnode.distanceTo(this.firstnode),
				i = null,
				r = null,
				t = u.options.icon.options.iconSize;
			t && (L.Util.isArray(t) ? (t[0] && (i = t[0]), t[1] && (r = t[1])) : (t.x && (i = t.x), t.y && (r = t.y)));
			i !== null && r !== null && (f = Math.max(i, r), e > f && (this._onMapClick(n), this.firstnode = this.secondnode))
		},
		_DragEnd: function() {
			this.firstnode = this.secondnode = null
		},
		_redowork: function() {},
		_onMapClick: function(n) {
			var t = n.latlng || n.target._latlng;
			this._createAnchor(t);
			this.node.setLatLng(t);
			this.nodes++;
			this.options.description = [{
				name: "未命名",
				alias: "",
				content: ""
			}]
		},
		_firefeatureCreated: function(n) {
			this._map.fire("featureCreated", {
				feature: n,
				featuretype: this.type
			})
		}
	});
	L.DrawPad.Point = L.DrawPad.extend({
		addHooks: function() {
			this.type = 0;
			L.DrawPad.prototype.addHooks.call(this);
			var t = this.nodeoptions,
				n = this.options;
			n.icon ? t.icon = n.icon : (t.icon = null, t.markerno = n.markerno, t.markertype = n.markertype);
			n.iconSize && (t.iconSize = n.iconSize);
			n.iconAnchor && (t.iconAnchor = n.iconAnchor);
			n.shadowSize && (t.shadowSize = n.shadowSize);
			n.shadowAnchor && (t.shadowAnchor = n.shadowAnchor);
			n.propagation = !1;
			n.rank = 0;
			n.interactive = !0;
			n.draggable = !0
		},
		_onMapClick: function(n) {
			L.DrawPad.prototype._onMapClick.call(this, n);
			var t = this.nodeoptions,
				i = this.options,
				r = this.node;
			r && this._firefeatureCreated(i.keep ? this._map.addPointFeature(r.getLatLng(), L.extend(L.extend({}, i), {
				icon: t.icon,
				markerno: t.markerno,
				markertype: t.markertype
			})) : null)
		}
	});
	L.DrawPad.Image = L.DrawPad.Point.extend({
		addHooks: function() {
			L.DrawPad.Point.prototype.addHooks.call(this);
			this.type = 3;
			this.toolTipEnable = !1
		},
		_onMapClick: function(n) {
			var t;
			if(L.DrawPad.prototype._onMapClick.call(this, n), t = this.node, t) {
				var i = this._map,
					r = t.getLatLng(),
					u = i.containerPointToLatLng(i.latLngToContainerPoint(r)._add({
						x: t._icon.offsetWidth,
						y: t._icon.offsetHeight
					}));
				L.setOptions(this, {
					url: this.options.icon.options.iconUrl
				});
				this._firefeatureCreated(this.options.keep ? i.addImageFeature(L.latLngBounds(L.latLng({
					lng: r.lng,
					lat: u.lat
				}), L.latLng({
					lng: u.lng,
					lat: r.lat
				})), this.options) : null)
			}
		}
	});
	L.DrawPad.Line = L.DrawPad.extend({
		icon: L.icon({
			iconUrl: L.iPath + "pen.gif",
			iconSize: [15, 15],
			iconAnchor: [8, 8]
		}),
		guidelineoptions: {
			rank: 9,
			color: "#ff0000",
			opacity: 1,
			dashArray: "10,8"
		},
		addHooks: function() {
			var i, n, t;
			this.type = 1;
			L.DrawPad.prototype.addHooks.call(this);
			i = this.nodeoptions;
			i.icon = this.icon;
			n = this.options;
			t = this.guidelineoptions;
			NULL(n.color) || (t.color = n.color);
			NULL(n.weight) || (t.weight = n.weight);
			NULL(n.opacity) || (t.opacity = n.opacity);
			this.vertex = []
		},
		removeHooks: function() {
			var n = this._map;
			n && (this.guideline && n.removeFeature(this.guideline), this.geometry && n.removeFeature(this.geometry));
			delete this.vertex;
			L.DrawPad.prototype.removeHooks.call(this)
		},
		_onMapMouseMove: function(n) {
			var i, t, r;
			L.DrawPad.prototype._onMapMouseMove.call(this, n);
			this.node && (i = this.node.getLatLng(), t = this.endvertex, t && (r = this.type === 1 ? [t, i] : [t, i, this.vertex[0]], this.guideline ? this.guideline.setLatLngs(r) : this.guideline = this._map.addLineFeature(r, this.guidelineoptions)))
		},
		_onMapClick: function(n) {
			L.DrawPad.prototype._onMapClick.call(this, n);
			this.node && (this.vertex.push(this.endvertex = this.node.getLatLng()), this.geometry ? this.geometry.setLatLngs(this.type === 1 ? this.vertex : [this.vertex]) : this.geometry = this._createGeometry(this.vertex), this.dirty = !0)
		},
		_createGeometry: function(n) {
			var t = this.options,
				i = this._map;
			return this.type === 1 ? i.addLineFeature(n, t) : i.addPolygonFeature(L.Util.Polygons(n), t)
		},
		_redowork: function() {
			var t = this._map,
				i = this.type,
				n = !0,
				r = this.vertex.length;
			i === 1 && r < 2 ? n = !1 : i === 2 && r < 3 && (n = !1);
			n && this._firefeatureCreated(this.options.keep ? this._createGeometry(this.vertex.slice(0)) : null);
			t.removeFeature(this.geometry);
			this.geometry = null;
			this.vertex = [];
			this.guideline && (t.removeFeature(this.guideline), this.guideline = null);
			this.endvertex = null;
			this.dirty = !1
		}
	});
	L.DrawPad.Polygon = L.DrawPad.Line.extend({
		addHooks: function() {
			L.DrawPad.Line.prototype.addHooks.call(this);
			this.type = 2
		}
	});
	L.PopupBox = L.Class.extend({
		includes: L.Mixin.Events,
		statics: {
			THIS: null,
			zIndex: 65535
		},
		options: {
			zIndex: null,
			model: !0,
			multiple: !1,
			top: null,
			left: null,
			width: null,
			height: null,
			padding: 2,
			color: "#f2f2f2",
			borderColor: "#2a8bd7",
			borderWidth: 1,
			borderRadius: 4,
			borderShadowOpacity: 85,
			borderShadowRing: 5,
			maskColor: "gray",
			maskOpacity: 45,
			zoomButton: !0,
			draggable: !0,
			expand: !0,
			full: !1,
			closeButton: !0,
			closeImage: L.iPath + "delete.gif",
			closeImageSize: {
				width: 16,
				height: 16
			},
			titleHeight: 24,
			age: 0
		},
		initialize: function(i, r, u) {
			var f = this.id = "_" + L.stamp(this),
				o, e;
			u = L.setOptions(this, u);
			u.multiple === !1 && (L.PopupBox.THIS && L.PopupBox.THIS.close(), L.PopupBox.THIS = this);
			this.full = u.full;
			t.body.appendChild(this._frame());
			o = null;
			u.model && (o = this.Mask = _("popupboxMask" + f));
			var c = this.Popup = _("popupbox" + f),
				a = this.LeftTop = _("popupboxLeftTop" + f),
				v = this.Top = _("popupboxTop" + f),
				y = this.RightTop = _("popupboxRightTop" + f),
				p = this.Left = _("popupboxLeft" + f);
			this.Center = _("popupboxCenter" + f);
			u.zoomButton && (this.ShowControl = _("popupboxShowControl" + f));
			var l = this.Show = u.zoomButton ? _("popupboxShow" + f) : null,
				s = this.Title = _("popupboxTitle" + f),
				h = null;
			u.closeButton && (h = this.Close = _("popupboxClose" + f));
			var w = this.Html = _("popupboxHtml" + f),
				b = this.Right = _("popupboxRight" + f),
				k = this.LeftBottom = _("popupboxLeftBottom" + f),
				d = this.Bottom = _("popupboxBottom" + f),
				g = this.RightBottom = _("popupboxRightBottom" + f),
				nt = this._size();
			if(i && (s.innerHTML = i), r && (w.innerHTML = r), c.style.display = "block", u.model && (o.style.display = "block"), this.options.expand = !this.options.expand, this._show(), h && L.DomEvent.on(h, "click", this.close, this), u.model && L.DomEvent.on(o, "click", this.close, this), l && L.DomEvent.on(l, "click", this._show, this), u.draggable) {
				L.DomEvent.on(n, "resize", this._onResize, this);
				this.Title_drag = new L.Draggable(c, s);
				this.LeftBottom_drag = new L.Draggable(k);
				this.Left_drag = new L.Draggable(p);
				this.LeftTop_drag = new L.Draggable(a);
				this.Top_drag = new L.Draggable(v);
				this.RightTop_drag = new L.Draggable(y);
				this.Right_drag = new L.Draggable(b);
				this.Bottom_drag = new L.Draggable(d);
				this.RightBottom_drag = new L.Draggable(g);
				this.LeftBottom_drag._updatePosition = this.Left_drag._updatePosition = this.RightTop_drag._updatePosition = this.Top_drag._updatePosition = this.LeftTop_drag._updatePosition = this.Right_drag._updatePosition = this.Bottom_drag._updatePosition = this.RightBottom_drag._updatePosition = function(n) {
					this.fire("predrag", n);
					this.fire("drag", n)
				};
				e = this.DragEvent = {
					dragstart: this._onDragStart,
					drag: this._onDrag
				};
				this.Title_drag.on(e, this);
				this.Title_drag.enable();
				this.LeftBottom_drag.on(e, this);
				this.LeftBottom_drag.enable();
				this.Left_drag.on(e, this);
				this.Left_drag.enable();
				this.Bottom_drag.on(e, this);
				this.Bottom_drag.enable();
				this.RightBottom_drag.on(e, this);
				this.RightBottom_drag.enable();
				this.Right_drag.on(e, this);
				this.Right_drag.enable();
				this.LeftTop_drag.on(e, this);
				this.LeftTop_drag.enable();
				this.Top_drag.on(e, this);
				this.Top_drag.enable();
				this.RightTop_drag.on(e, this);
				this.RightTop_drag.enable();
				L.DomEvent.on(s, "click", this.onDblclick, this)
			}
			this.open = !0;
			u.age > 0 && setTimeout(L.bind(this.close, this), u.age);
			this.fire("open", nt)
		},
		_size: function() {
			var n = this.options,
				o = {
					width: t.documentElement.clientWidth,
					height: t.documentElement.clientHeight
				},
				l = n.titleHeight,
				i = n.borderShadowRing * 1,
				r = n.padding * 1,
				u = n.borderWidth * 1,
				h = this.full,
				a = this.Popup,
				s = a.style,
				f, e, c;
			return h ? (s.transform = null, n.width = (f = o.width) - i - u - r - r - u - i) : (f = n.width, f = Math.max((n.width = f == null || f < 0 ? o.width * .6 : parseInt(f)) + i + u + r + r + u + i, n.closeImageSize.width * (n.zoomButton ? 2 : 1) + i + u + r + r + u + i)), h ? n.height = (e = o.height) - i - u - l - 1 - r - r - u - i : (e = n.height, e = (n.height = e == null || e < 0 ? o.height * .8 : parseInt(e)) + i + u + l + 1 + r + r + u + i), c = this.Html, c.style.height = n.height + "px", c.style.width = n.width + "px", s.top = (n.top = h ? 0 : n.top === null ? (o.height - e) / 2 : parseInt(n.top)) + "px", s.left = (n.left = h ? 0 : n.left === null ? (o.width - f) / 2 : parseInt(n.left)) + "px", s.width = f + "px", s.height = e + "px", {
				outer: {
					top: n.top,
					left: n.left,
					width: f,
					height: e
				},
				inner: {
					width: n.width,
					height: n.height
				}
			}
		},
		_onResize: function() {
			if(this.full) {
				this.__onResize && clearTimeout(this.__onResize);
				var n = this;
				n.__onResize = setTimeout(L.bind(function() {
					clearTimeout(n.__onResize);
					n.__onResize = null;
					n._size()
				}, n), 10)
			}
		},
		onDblclick: function(n) {
			var i, t, r;
			L.DomEvent.stop(n);
			i = this.options;
			i.expand && (t = this, t._DoOnce ? (clearTimeout(t._DoOnce), t._DoOnce = null, r = t.Popup.style, t.full ? (t.full = !1, i.top = t.top, i.left = t.left, i.width = t.width, i.height = t.height, r.transform = t.transform) : (t.full = !0, t.top = i.top, t.left = i.left, t.width = i.width, t.height = i.height, t.transform = r.transform), t._size()) : t._DoOnce = setTimeout(L.bind(function() {
				t._DoOnce = null
			}, t), 250))
		},
		_show: function() {
			var n = this.options,
				i = n.borderShadowRing,
				f = n.padding * 1,
				r = n.titleHeight,
				u = n.borderWidth * 1,
				t = n.expand = !n.expand;
			this.Center.style.height = t ? null : r + "px";
			this.Popup.style.height = (t ? n.height + 2 * f + 1 : 0) + (r + i + u + i + u) + "px";
			this.Show && (this.ShowControl.src = L.iPath + "arrow/1" + (t ? "0" : "1") + ".gif");
			this.fire("zoom" + (t ? "In" : "Out"))
		},
		_onDragStart: function(n) {
			this._oldPos = n.target._startPoint
		},
		_onDrag: function(n) {
			function e(n) {
				n !== 1 && (f.top = (t.top = parseInt(f.top) + i.y) + "px");
				n !== 0 && (f.left = (t.left = parseInt(f.left) + i.x) + "px")
			}
			var t = this.options,
				f, i, l, a, o;
			if(t.expand) {
				l = n.target._newPos.subtract(n.target._startPos).add(n.target._startPoint);
				i = l.subtract(this._oldPos);
				a = this.full = !1;
				o = this.Popup;
				f = o.style;
				var r = o.clientHeight,
					u = o.clientWidth,
					v = n.target._element.id.replace(/popupbox(.*?)_.*/img, "$1");
				switch(v) {
					case "Right":
						u += i.x;
						break;
					case "Bottom":
						r += i.y;
						break;
					case "RightBottom":
						r += i.y;
						u += i.x;
						break;
					case "LeftBottom":
						r += i.y;
						u -= i.x;
						u > 0 && r > 0 && e(1);
						break;
					case "RightTop":
						r -= i.y;
						u += i.x;
						u > 0 && r > 0 && e(0);
						break;
					case "Left":
						u -= i.x;
						u > 0 && e(1);
						break;
					case "Top":
						r -= i.y;
						r > 0 && e(0);
						break;
					case "LeftTop":
						r -= i.y;
						u -= i.x;
						u > 0 && r > 0 && e(2);
						break;
					default:
						a = !0;
						t.top = parseInt(f.top);
						t.left = parseInt(f.left)
				}
				if(a) this.fire("Move", {
					top: t.top,
					left: t.left
				});
				else if(u > 0 && r > 0) {
					var s = t.borderShadowRing * 1,
						h = t.padding * 1,
						c = t.borderWidth * 1;
					f.height = r + "px";
					f.width = u + "px";
					this.Html.style.width = (t.width = u - s - c - h - h - c - s) + "px";
					this.Html.style.height = (t.height = r - s - c - t.titleHeight - 1 - h - h - c - s) + "px";
					this._oldPos = l;
					this.fire(v, {
						width: t.width,
						height: t.height
					})
				}
			}
		},
		_frame: function() {
			var n = this.options,
				e = n.zIndex,
				o, v;
			e !== null && e > L.PopupBox.zIndex && (L.PopupBox.zIndex = e);
			var u = n.borderShadowRing * 1,
				y = n.padding * 1,
				i = this.id,
				f = n.maskOpacity,
				s = n.borderShadowOpacity * .01,
				h = n.titleHeight,
				p = n.borderWidth * 1,
				c = n.closeImageSize.width,
				l = n.closeImageSize.height,
				a = (h - l) / 2,
				r = [];
			for(n.model && r.push('<div id="popupboxMask' + i + '" style="display:none;position:absolute;top:0%;left:0%;width:100%;height:100%;background-color:' + n.maskColor + ";z-index:" + L.PopupBox.zIndex++ + ";-moz-opacity:" + f * .01 + ";opacity:" + f * .01 + ";-webkit-filter:alpha(opacity=" + f + ");-moz-filter:alpha(opacity=" + f + ");-o-filter:alpha(opacity=" + f + ");filter:alpha(opacity=" + f + ');"><\/div>'), r.push('<table id="popupbox' + i + '" cellpadding="0" cellspacing="0" border="0" style="display:none;position:absolute;z-index:' + L.PopupBox.zIndex++ + ';overflow:hidden;">'), r.push('<tr style="height:' + u + 'px"><td id="popupboxLeftTop' + i + '" style="width:' + u + "px;" + (n.draggable ? "cursor:nw-resize;" : "") + '"><\/td><td id="popupboxTop' + i + '"' + (n.draggable ? ' style="cursor:n-resize"' : "") + '><\/td><td id="popupboxRightTop' + i + '" style="width:' + u + "px;" + (n.draggable ? "cursor:ne-resize;" : "") + '"><\/td><\/tr>'), r.push('<tr><td id="popupboxLeft' + i + '"' + (n.draggable ? ' style="cursor:w-resize"' : "") + '><\/td><td id="popupboxMiddle' + i + '"><div id="popupboxCenter' + i + '" style="border:' + p + "px solid " + n.borderColor + ";background-color:" + n.color + ";border-radius:" + n.borderRadius + "px;-webkit-box-shadow:0 1px " + u + "px rgba(0,0,0," + s + ");box-shadow:0 1px " + u + "px rgba(0,0,0," + s + ');font-size:12px;font-family:Verdana,Geneva,Tahoma,sans-serif;overflow:hidden;"><table cellpadding="0" cellspacing="0" style="width:100%;height:100%" border="0"><tr><td style="height:' + h + 'px;border-style:none none solid none;border-width:1px;border-color:#999999;"><table cellpadding="0" cellspacing="0" style="width:100%;"><tr>'), n.zoomButton && r.push('<td id="popupboxShow' + i + '" style="padding:' + a + "px;cursor:pointer;width:" + c + 'px;" align="center" valign="middle"><img id="popupboxShowControl' + i + '" alt="" style="display:block" src="' + L.iPath + "arrow/1" + (n.expand ? "0" : "1") + '.gif" /><\/td>'), r.push('<td id="popupboxTitle' + i + '" style="cursor:default;text-align:center;vertical-align:middle;white-space:nowrap;overflow:hidden;"><\/td>'), n.closeButton && r.push('<td id="popupboxClose' + i + '" style="padding:' + a + "px;cursor:pointer;width:" + c + "px;height:" + l + 'px;"><img alt="" src="' + n.closeImage + '" style="display:block"/><\/td>'), r.push('<\/tr><\/table><\/td><\/tr><tr><td style="vertical-align:top;padding:' + y + 'px;background-color:#dfdfdf;"><div id="popupboxHtml' + i + '" style="overflow:auto;-webkit-overflow-scrolling:touch;"><\/div><\/td><\/tr><\/table><\/div><\/td><td id="popupboxRight' + i + '"' + (n.draggable ? ' style="cursor:e-resize"' : "") + "><\/td><\/tr>"), r.push('<tr style="height:' + u + 'px"><td id="popupboxLeftBottom' + i + '"' + (n.draggable ? ' style="cursor:sw-resize"' : "") + '><\/td><td id="popupboxBottom' + i + '"' + (n.draggable ? ' style="cursor:s-resize"' : "") + '><\/td><td id="popupboxRightBottom' + i + '"' + (n.draggable ? ' style="cursor:se-resize"' : "") + "><\/td><\/tr>"), r.push("<\/table>"), o = t.createElement("div"), o.innerHTML = r.join(""), r = t.createDocumentFragment(); v = o.firstChild;) r.appendChild(v);
			return r
		},
		close: function() {
			var i = this.options,
				t;
			this.open && (i.draggable && (L.DomEvent.off(n, "resize", this._onResize, this), t = this.DragEvent, this.Title_drag.off(t, this), this.Title_drag.disable(), this.Bottom_drag.off(t, this), this.Bottom_drag.disable(), this.RightBottom_drag.off(t, this), this.RightBottom_drag.disable(), this.Right_drag.off(t, this), this.Right_drag.disable(), this.LeftTop_drag.off(t, this), this.LeftTop_drag.disable(), this.Top_drag.off(t, this), this.Top_drag.disable(), this.RightTop_drag.off(t, this), this.RightTop_drag.disable(), this.Left_drag.off(t, this), this.Left_drag.disable(), this.LeftBottom_drag.off(t, this), this.LeftBottom_drag.disable(), L.DomEvent.off(this.Title, "click", this.onDblclick, this)), i.closeButton && L.DomEvent.off(this.Close, "click", this.close, this), i.model && L.DomEvent.off(this.Mask, "click", this.close, this), this.Show && L.DomEvent.off(this.Show, "click", this._show, this), this.Title.innerHTML = this.Html.innerHTML = null, this.Popup.parentNode.removeChild(this.Popup), this.options.model && this.Mask.parentNode.removeChild(this.Mask), this.fire("close"), i.multiple === !1 && (L.PopupBox.THIS = null))
		}
	});
	L.popupBox = function(n, t, i) {
		return new L.PopupBox(n, t, i)
	}
})(window, document)